SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `body` varchar(512) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `read` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ew_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ew_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `recipient_balance` decimal(15,0) NOT NULL,
  `sender_balance` decimal(15,0) NOT NULL,
  `description` varchar(255) NOT NULL,
  `proof` varchar(128) NOT NULL,
  `deposit_date` timestamp NULL DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `levels` (
  `id` varchar(10) NOT NULL,
  `pay` int(11) NOT NULL,
  `pin` int(11) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `upline` int(11) DEFAULT NULL,
  `downline_count` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `profit` int(11) DEFAULT NULL,
  `nett` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `mailbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` varchar(1024) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `sender_trash` tinyint(1) NOT NULL,
  `recipient_trash` tinyint(4) NOT NULL DEFAULT '0',
  `starred` tinyint(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `member_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `account_num` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_phone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` varchar(1024) NOT NULL,
  `class` varchar(64) NOT NULL,
  `target` varchar(64) NOT NULL,
  `publish_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'register-member', 'Register New Member', 'Register New Member', '2015-11-22 10:00:00', '2015-11-22 10:00:00'),
(2, 'edit-member', 'Edit Other Members Profile', 'Edit Other Members Profile', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2);

CREATE TABLE IF NOT EXISTS `pins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` varchar(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `available` tinyint(1) NOT NULL,
  `used` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `pin_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `approved` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Site Administrator', 'Has all privileges the site can offer users', '2015-11-06 19:24:49', '2015-11-06 19:24:49'),
(2, 'manager', 'Manager', 'Can manage and register other members', '2015-11-06 19:24:49', '2015-11-06 19:24:49'),
(3, 'member', 'Member', 'Ordinary registered member', '2015-11-06 19:24:49', '2015-11-06 19:24:49');

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `settings` (
  `slug` varchar(50) NOT NULL,
  `title` varchar(128) NOT NULL,
  `type` varchar(12) NOT NULL,
  `value` varchar(512) NOT NULL,
  `options` varchar(1024) NOT NULL,
  `order` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`slug`, `title`, `type`, `value`, `options`, `order`, `description`, `created_at`, `updated_at`) VALUES
('auto_upgrade', 'Can Members Be Upgraded Automatically?', 'boolean', '1', '', 8, NULL, '2015-12-28 13:09:44', '2016-01-04 07:17:23'),
('currency', 'Default Currency', 'text', ' Malaysian Ringgit', '', 4, NULL, '2015-12-24 08:02:02', '2016-01-04 07:17:23'),
('currency_symbol', 'Default Currency Symbol', 'text', 'MYR', '', 5, NULL, '2015-12-24 08:02:02', '2016-01-04 07:17:23'),
('max_member_lev', 'Highest Member Level', 'select', '1', '1,2,3,4,5,6,7,8,9,10', 8, 'The highest level a member can attain before they become manager', '2016-01-04 08:45:37', '2016-01-04 07:17:24'),
('pin_price', 'Pin Price', 'float', '30', '', 1, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('require_email_validation', 'New Member Email Validation Required?', 'boolean', '1', '', 2, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('server_timezone', 'Default Server Timezone', 'select', 'America/New_York', 'Africa/Lagos,Africa/Cairo,Africa/Addis_Ababa,America/Adak,America/New_York,Europe/London,Asia/Beirut,Asia/Bangkok,Asia/Kuala_Lumpur,Asia/Ho_Chi_Minh', 3, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('site_name', 'Name Of This Website', 'text', 'Matrix Logic', '', 0, NULL, '2015-12-24 07:21:31', '2016-01-04 07:17:23'),
('withdrawal_max', 'eWallet Withdrawal Maximum', 'text', '2000', '', 7, NULL, '2015-12-24 13:08:55', '2016-01-04 07:17:23'),
('withdrawal_min', 'eWallet Withdrawal Minimum', 'text', '500', '', 6, NULL, '2015-12-24 13:12:17', '2016-01-04 07:17:23');


CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_id` int(11) NOT NULL,
  `level` int(1) DEFAULT '0',
  `upline` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `downline` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `users` (`id`, `username`, `fname`, `lname`, `gender`, `email`, `phone`, `country`, `manager_id`, `level`, `upline`, `downline`, `active`, `password`, `remember_token`, `last_activity`, `created_at`, `updated_at`) VALUES
(-1, 'system', 'Matrix', 'Logic', 'female', 'system@matrix-logic.com', '', NULL, 0, 0, '', '', 0, 'no one can log in as system', 'no one can log in as system', '0000-00-00 00:00:00', '2015-10-30 13:59:36', '2015-12-16 01:41:08'),
(0, 'matrix-logic', 'Matrix', 'Logic', 'female', 'website@matrix-logic.com', '', NULL, 0, 0, '', '', 0, 'no one can log in as system', 'no one can log in as system', '0000-00-00 00:00:00', '2015-10-30 13:59:36', '2015-12-18 15:09:13'),
(1, 'kaydee', 'Ken', 'Dan', 'male', 'ken@dan.com', '', NULL, 0, 1, '', '', 1, '$2y$10$ktX5MR1uSXOtc.Cy4DBmhOy2/5SkM89QRj/ZmOfUG37iFbN19WNCa', 'II1Uez4ncbWGcqZptHFREOYouqlvP2jTkySYz6YHyBc0ZdN8XbF7ImmguqHo', '2016-02-03 11:27:54', '2015-10-30 13:59:36', '2016-02-03 11:27:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
