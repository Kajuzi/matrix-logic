                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h2 class="panel-title">eWallet Balance: {{ number_format(balance(\Auth::id())) }}</h2>
                                </div>
                                <div class="panel-body">
                                    <ul class="menu-ul">
                                        <li><a class="btn btn-rounded btn-danger" href="#" onclick="return showForm('#transfer-form')">Top Up eWallet&nbsp;&nbsp; <i class="fa fa-plus"></i></a></li>
                                        <li><a class="btn btn-rounded btn-danger" href="#" onclick="return showForm('#pinpurchase-form')">Buy Pins&nbsp;&nbsp; <i class="fa fa-star"></i></a></li>
                                        <li><a class="btn btn-rounded btn-danger" href="#" onclick="return showForm('#ewallettopup-form')">Transfer Funds&nbsp;&nbsp; <i class="fa fa-exchange"></i></a></li>
                                        <li><a class="btn btn-rounded btn-danger" href="{{ url('ewallet/download-pdf')}}">Download As PDF&nbsp;&nbsp; <i class="fa fa-file-pdf-o"></i></a></li>
                                        <li><a class="btn btn-rounded btn-success" href="#" onclick="return showForm('#withdrawal-form')">Withdraw Funds&nbsp;&nbsp; <i class="fa fa-download"></i></a></li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    
                                </div>
                            </div>