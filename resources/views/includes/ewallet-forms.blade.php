                            <div id="transfer-form" class="col-md-12 hidden-form">
                                <div class="">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Top Up eWallet Account</h4>
                                    </div>
                                    <div class="panel-body">
                                        {!! Form::open(['url' => 'ewallet/proof', 'files' => true, 'class'=>'form-horizontal', 'method'=>'POST']) !!}
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-2 control-label">Amount</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="amount" id="amount" placeholder="How much do you want to top up?" type="text">
                                                    <p id='amount-info' class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="deposit_date" class="col-sm-2 control-label">Date of Deposit</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control date-picker" name="deposit_date" id="deposit_date" placeholder="yyyy-mm-dd hh:mm" type="text">
                                                    <p id='deposit_date-info' class="help-block">Please make sure you enter the date in this form: {{ date('Y-m-d H:i') }}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="proof" class="col-sm-2 control-label">Proof</label>
                                                <div class="col-sm-10">
                                                    {!! Form::file('proof', array('class' => 'form-control', 'id'=>'proof-file', 'onchange'=>"checkfile()", 'accepted'=>"application/pdf, image/jpeg, image/png")) !!}
                                                    <p id='proof-info' class="help-block">Only PDF and JPEG allowed</p>
                                                </div>
                                            </div>
                                            {!! Form::hidden('top-up-ready', false) !!}
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker"><span><input type="checkbox" name="tncz"></span></div> I agree with Matrix Logic Terms  &amp; Conditions
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-success">Upload Proof!</button>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div id="pinpurchase-form" class="col-md-12 hidden-form">
                                <div class="">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Buy Pins</h4>
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::open(array('url'=>url('pins/buy'), 'method'=>'POST', 'class' => 'form-horizontal')) !!}
                                        <div class="form-group">
                                            <label for="quantity" class="col-sm-2 control-label">Number of PIN</label>
                                            <div class="col-sm-10">
                                                <select class="form-control m-b-sm" name="quantity">
                                                    @for($i=1; $i<=$max_pins; $i++)
                                                    <option>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                <p class="help-block">You can buy up to {{ $max_pins }} pins</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="password" id="password" placeholder="Password" type="password" required="required">
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Send Request</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div id="ewallettopup-form" class="col-md-12 hidden-form">
                                <div class="">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Transfer eWallet Funds</h4>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label for="username" class="col-sm-2 control-label">Username</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group m-b-sm">
                                                        <input class="form-control" id="transfer-username" name="username" placeholder="Recipient's username" type="text" required='required'>
                                                        <span class="input-group-btn">
                                                            <button id="check-recipient-info" class="btn btn-default" type="button">Check</button>
                                                        </span>
                                                    </div>
                                                    <p id='recipient-info' class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="amount" class="col-sm-2 control-label">Amount</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="amount" id="amount" placeholder="How much do you want to transfer?" type="text">
                                                    <p id='amount-info' class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="password" class="col-sm-2 control-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" id="password" placeholder="Your Password" type="password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker"><span><input type="checkbox" name="tncz"></span></div> I agree with Matrix Logic Terms  &amp; Conditions
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-success">Transfer</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="withdrawal-form" class="col-md-12 hidden-form">
                                <div class="">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Withdraw Funds</h4>
                                    </div>
                                    <div class="panel-body">
                                    @if(balance($id) < settings('withdrawal_min'))
                                        <h3 class="text-danger">You do not have enough funds in your eWallet account. You need at least {{settings('currency_symbol').' '.settings('withdrawal_min') }} to be able to withdraw </h3>
                                    @else
                                    {!! Form::open(array('url'=>url('ewallet/withdraw'), 'method'=>'POST', 'class' => 'form-horizontal')) !!}
                                        <div class="form-group">
                                            <label for="quantity" class="col-sm-2 control-label">Amount</label>
                                            <div class="col-sm-10">
                                                {!! Form::text('amount', settings('withdrawal_min'), ['class' => 'form-control']) !!}
                                                <p class="help-block">You can withdraw up to {{ settings('currency_symbol').' '.$withdrawal_max }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="password" id="password" placeholder="Password" type="password" required="required">
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Send Request</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                    @endif
                                    </div>
                                </div>
                            </div>