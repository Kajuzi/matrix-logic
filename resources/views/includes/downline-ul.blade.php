                                            <ul class="jstree-children" role="group">
                                                @foreach($downline as $ki => $c)
		                                        <li class="jstree-node  jstree-{{ ($c->hasDownline)?'closed':'leaf' }}" id="j1_1" data-uid="{{ $c->id }}" aria-expanded="false" aria-labelledby="j1_1_anchor" aria-level="1" aria-selected="false" role="treeitem">
		                                            <i role="presentation" class="jstree-icon jstree-{{ ($c->hasDownline)?'closed':'ocl' }}"></i>
		                                            <a id="j1_1_anchor" tabindex="-1" href="#" class="jstree-anchor">
		                                                <i role="presentation" class="jstree-icon jstree-themeicon fa fa-user icon-state-info icon-md jstree-themeicon-custom"></i>{!! $c->username.' <strong class="text-info">'.$c->fname.' '.$c->lname.'</strong>' !!}
		                                            </a>
		                                        </li>
                                                @endforeach
                                            </ul>