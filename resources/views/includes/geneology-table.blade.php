
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Full Name</th>
                                                    <th>Rank</th>
                                                    <th>Country</th>
                                                    <th>&nbsp</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($downline as $k => $d)
                                                <tr>
                                                    <th scope="row">{{ $k + 1 }}</th>
                                                    <td>{{ $d['username'] }}</td>
                                                    <td>{{ $d['fname'].' '.$d['lname'] }}</td>
                                                    <td>Member</td>
                                                    <td>{{ $d['country'] }}</td>
                                                    <td><a href="{{ url('members/profile/'.$d['id']) }}">View Profile</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>