<?php  
    $unread     = \App\Models\Mailbox::countUnread(Auth::id());
    $inbox      = \App\Models\Mailbox::inbox(Auth::id());
    $alerts     = alerts();
    $countAlerts= \App\Models\Alert::countUnread();
?>
<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Matrix Logic</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Matrix Logic" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Kajuzi" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="{{ asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/plugins/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">	
        @yield('css')
        
        <!-- Theme Styles -->
        <link href="{{ asset('assets/css/modern.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
        
        <script src="{{ asset('assets/plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body class="page-header-fixed page-horizontal-bar">
        <div class="overlay"></div>
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner container">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ url('/') }}" class="logo-text"><span>Matrix Logic</span></a>
                    </div><!-- Logo Box -->
                    <div class="topmenu-outer">
                        <div class="top-menu">
                         
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-envelope"></i>@if($unread)<span class="badge badge-danger pull-right">{{ $unread }}</span>@endif</a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have {{ ($unread)?$unread:'no' }} new  message{{ ($unread>1)?'s':'' }}!</p></li>
                                        @if($unread)
                                        <li class="dropdown-menu-list slimscroll messages">
                                            @foreach($inbox as $m)
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="{{ url('mailbox/view/'.$m->id) }}">
                                                        <div class="msg-img"><i class="fa fa-envelope icon-primary"></i><!-- <img class="img-circle" src="{{ asset('assets/images/avatar2.png') }}" alt=""> --></div>
                                                        <p class="msg-name">{{ \App\User::find($m->sender_id)->username }}</p>
                                                        <p class="msg-text">{{ $m->title }}</p>
                                                        <p class="msg-time">{{ \App\Models\Handy::ago($m->created_at) }}</p>
                                                    </a>
                                                </li>
                                            </ul>
                                            @endforeach
                                        </li>
                                        @endif
                                        <li class="drop-all"><a href="{{ url('mailbox/inbox') }}" class="text-center">All Messages</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-bell"></i>@if($countAlerts)<span id="countAlerts" class="badge badge-danger pull-right">{{ $countAlerts }}</span>@endif</a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have {{ ($countAlerts)?$countAlerts:'no' }} new alert{{ ($countAlerts==1)?'':'s' }} !</p></li>
                                        @if($countAlerts)
                                        <li class="dropdown-menu-list slimscroll tasks">
                                            <ul class="list-unstyled">
                                            @foreach($alerts as $a)
                                                <li onclick="return readAlert({{ $a->id }})">
                                                    <a href="{{ url($a->action) }}">
                                                        <!-- <div class="task-icon badge badge-success text-center">&nbsp;</div> -->
                                                        <span class="badge badge-roundless badge-default pull-right">{{ $a->when }}</span>
                                                        <p class="task-details text-{{ $a->type or 'info' }}">{{ $a->body }}</p>
                                                    </a>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </li>
                                        @endif
                                        <li class="drop-all"><a href="#" class="text-center">All Alerts</a></li>
                                        <li onclick="return readAlert('all')" class="drop-all"><a href="#" class="text-center">Mark All As Read</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">{{ Auth::user()->fname }} {{ Auth::user()->lname }}<i class="fa fa-angle-down"></i></span><span class="user-avatar-icon"><i class="fa fa-user"></i></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-list" role="menu">
                                        <li role="presentation"><a href="{{ url('members/profile/'.$id) }}"><i class="fa fa-user"></i>Profile</a></li>
                                        <li role="presentation"><a href="{{ url('mailbox/inbox') }}"><i class="fa fa-envelope"></i>Inbox</a></li>
                                        <li role="presentation"><a href="{{ url('logout') }}"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                                    </ul>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar horizontal-bar">
                <div class="page-sidebar-inner">
                    <ul class="menu accordion-menu">
                        <li class="nav-heading"><span>Navigation</span></li>
                        <li><a href="{{ url('dashboard') }}"><span class="menu-icon icon-speedometer"></span><p>Dashboard</p></a></li>
                        <li><a href="{{ url('members/profile/'.$id) }}"><span class="menu-icon icon-user"></span><p>Profile</p></a></li>
                        <li class="nav-heading"><span>Features</span></li>
                        <li class="droplink"><a href="#"><span class="menu-icon icon-briefcase"></span><p>Accounts</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('ewallet/show/'.Auth::id()) }}">eWallet</a></li>
                                <li><a href="{{ url('pins') }}">Pin History</a></li>
                                <li><a href="{{ url('ewallet/commission') }}">Commission</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('members/geneology/'.\Auth::id()) }}"><span class="menu-icon icon-users"></span><p>Sponsor Geneology</p></a></li>
                        <li class="droplink"><a href="#"><span class="menu-icon icon-list"></span><p>Transactions</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('ewallet/show/'.Auth::id()) }}">eWallet History</a></li>
                                <li><a href="{{ url('pins/pin-purchase-requests/'.Auth::id()) }}">Pin Purchace Requests</a></li>
                            </ul>
                        </li>
                        <li class="droplink"><a href="#"><span class="menu-icon icon-envelope-open"></span><p>Mailbox</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('mailbox/inbox') }}">Inbox</a></li>
                                <li><a href="{{ url('mailbox/compose') }}">Compose</a></li>
                            </ul>
                        </li>
                        @role('admin')
                        <li><a href="{{ url('admin/index') }}"><span class="menu-icon icon-wrench"></span><p>Admin Panel</p></a></li>
                        @endrole
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
            @yield('content')
                <div class="page-footer">
                    <div class="container">
                        <p class="no-s">2015 &copy; Matrix Logic<span class="pull-right">Site by <a href="http://kajuzi.com">Kajuzi</a></span></p>
                    </div>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <div class="cd-overlay"></div>	

        <!-- Javascripts -->
        <script src="{{ asset('assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/classie/classie.js') }}"></script>
        <script src="{{ asset('assets/plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/3d-bold-navigation/js/main.js') }}"></script>
        @yield('js')
        <script src="{{ asset('assets/js/modern.min.js') }}"></script>

        @if(\Session::has('toastr'))
        <script type="text/javascript" src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                Command: toastr['{{ \Session::get('toastr')['type'] }}']("", "{{ \Session::get('toastr')['msg'] }}")

                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "7000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            });
        </script>
        @endif      
        <script>            
            function readAlert (id) {
                $.get('{{ url('alert/mark-as-read') }}'+'/'+ id, { id: id }, function (data) {
                    if(data.status == 'success'){
                        if(data.unread)
                            $('#countAlerts').html(data.unread);
                        else
                            $('#countAlerts').remove();
                    }
                });
                return true;
            }
        </script>  
    </body>
</html>