<!DOCTYPE html>
<html>
    <head>
        <title>404 - Not found</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #777777;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .hugetitle {
                font-size: 72px;
            }

            .title {
                font-size: 52px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><span class="hugetitle">404</span><br>OOOOPS!!! <br/>That page seems to be unavailable.</div>
            </div>
        </div>
    </body>
</html>
