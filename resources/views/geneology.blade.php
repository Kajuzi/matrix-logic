@extends('layouts.master-layout-1')

@section('css')
        <link href="{{ asset('assets/plugins/jstree/themes/default/style.min.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
            <div class="page-inner">
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Sponsor Geneology</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title text-info">{{ $fname }} {{ $lname }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div id="lazy">
                                    </div>  
                                    <div class="text-center"><a href="javascript:void(0)"><h4 id="toggle-legend">Show/Hide Legend <i class="fa fa-angle-down"></i></h4></a></div>
                                    <div id="geneology-legend">
                                        <i class = 'icon-diamond  text-info'></i> Admin<br />
                                        <i class = 'icon-badge text-info-m'></i> Manager<br />
                                        <i class = 'fa fa-user text-l9'></i> Member Level 9 - 10<br />
                                        <i class = 'fa fa-user text-l6'></i> Member Level 6 - 8<br />
                                        <i class = 'fa fa-user text-l3'></i> Member Level 3 - 5<br />
                                        <i class = 'fa fa-user text-l1'></i> Member Level 1 - 2<br />
                                        <i class = 'fa fa-user text-gray'></i> Unofficial Member<br />
                                    </div>
                                </div>
                            </div>
                        </div> <!-- geneology tree -->
                        <div class="col-md-8">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">&nbsp;</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive" id="downline-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Full Name</th>
                                                    <th>Rank</th>
                                                    <th>Country</th>
                                                    <th>&nbsp</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($table as $k => $d)
                                                <tr>
                                                    <th scope="row">{{ $k + 1 }}</th>
                                                    <td>{{ $d['username'] }}</td>
                                                    <td>{{ $d['fname'].' '.$d['lname'] }}</td>
                                                    <td>Member</td>
                                                    <td>{{ $d['country'] }}</td>
                                                    <td><a href="{{ url('members/profile/'.$d['id']) }}">View Profile</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Main Wrapper -->
@stop

@section('js')
        <script src="{{ asset('assets/plugins/jstree/jstree.min.js') }}"></script>
		<script>
            //Listener
            $('#lazy')
            .on('changed.jstree', function (e, data) {
                var id = data.instance.get_node(data.selected).id;
                $.get("../geneology-table/" + id, {} )
                    .done(function(response) {
                        $("#downline-table").html(response);
                    })
                    .fail(function() {
                        $("#downline-table").html('<strong>System Internal Error</strong>');
                    }) ;
            })

            //Make the tree instance
            $('#lazy').jstree({
                'core' : {
                    'data' : {
                        "url" : "../geneology-tree/{{ $id }}",
                        "data" : function (node) {
                            return node;
                        }
                    }
                }
            });

            $('#toggle-legend').click(function(){
                $('#geneology-legend').toggle('slow');
            });
		</script>
@stop