@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Profile</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-{{ (issuperior($id))?'9':'12' }}">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">{{ (\Auth::id() == $id)?'Your':$fname."'s" }} profile</h4>
                                    <p class="pull-right">Last Activity : {{ ($last_activity == '0000-00-00 00:00:00')?"Never Been Active":ago($last_activity) }}</p>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open(['url'=>'members/update/'.$id, 'class'=>'form-horizontal', 'method'=>'post']) !!}
                                        <div class="form-group">
                                            <label for="email" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" id="email" placeholder="Email" type="email" value="{{ $email }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="username" class="col-sm-2 control-label">Username</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" id="username" placeholder="Choose a username" type="text" value="{{ $username }}" {{ $username?'disabled=""':'' }}>
                                                <p class="help-block">You're only allowed to change this once</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="username" class="col-sm-2 control-label">Rank</label>
                                            <div class="col-sm-10">
                                                <h4>{{ $rank }}</h4>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="fname" class="col-sm-2 control-label">First name</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="fname" placeholder="Your first name" type="text" value="{{ $fname }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lname" class="col-sm-2 control-label">Last name</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="lname" placeholder="Your last name" type="text" value="{{ $lname }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Gender</label>
                                            <div class="col-sm-10">
                                                {!! Form::select('gender', [''=>'Select Gender', 'male' => 'Male', 'female' =>'Female'], $gender, ['class'=>'form-control m-b-sm']) !!}
                                            </div>
                                        </div>                                        
                                        <hr>
                                        <div class="form-group">
                                            <label for="bank" class="col-sm-2 control-label">Bank</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name='title' id="bank" placeholder="Name of your bank" type="text" value="{{ ($bank)?$bank->title:'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bank_acc_num" class="col-sm-2 control-label">Bank Account Number</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="account_num" placeholder="Account Num" type="text" value="{{ ($bank)?$bank->account_num:'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bank" class="col-sm-2 control-label">Phone Number</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="account_phone" placeholder="The number linked with your bank account" type="text" value="{{ ($bank)?$bank->account_phone:'' }}">
                                            </div>
                                        </div>
                                        {!! Form::hidden('id', $id) !!}
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Save Changes</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        @if(issuperior($id))
                        <div class="col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h2 class="panel-title">User Accounts Info</h2>
                                </div>
                                <div class="panel-body">
                                    <h4 class="text-info">Pins : {{ \App\Models\Pin::countPins($id) }} ({{ \App\Models\Pin::countPins($id) }} available)</h4>
                                    <h4 class="text-info">eWallet Balance : {{ balance($id) }}</h4>
                                    <h4 class="text-info">Downline : {{ \App\Models\Member::GeneologyCount($id) }}</h4>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h2 class="panel-title">User Management Tasks</h2>
                                </div>
                                <div class="panel-body">
                                    <ul class="menu-ul">
                                        @if(\App\Models\Member::upgradable($id))
                                        <li><a class="btn btn-rounded btn-danger" href="{{ url('members/upgrade/'.$id) }}">Upgrade User &nbsp;&nbsp; <i class="fa fa-arrow-up"></i></a></li>
                                        @endif
                                        @role('admin')
                                        @if(\App\Models\Member::rank($id) == 'Manager')
                                            <li><a class="btn btn-rounded btn-danger" id="award-pins-btn" href="#">Award Pins&nbsp;&nbsp; <i class="fa fa-plus-square-o"></i></a></li>
                                            <div id="award-pins-form">
                                                {!! Form::open(array('url' => 'pins/award', 'id' => 'award-pins')) !!}
                                                    {!! Form::hidden('user_id', $id) !!}
                                                    {!! Form::text('quantity', 0, ['class'=>'form-control m-b-sm', 'id'=>'quantity']) !!}
                                                    {!! Form::submit('Award!', ['class'=>"btn btn-success pull-center m-b-sm", 'id'=>"award-submit" ]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        @endif
                                        <li>
                                            @if($active)
                                                <a class="btn btn-rounded btn-danger" href="{{ url('members/deactivate/'.$id) }}">Suspend The User&nbsp;&nbsp; <i class="fa fa-ban"></i></a>
                                            @else
                                                <a class="btn btn-rounded btn-success" href="{{ url('members/activate/'.$id) }}">Activate The User&nbsp;&nbsp; <i class="fa fa-check"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            <a class="btn btn-rounded btn-danger" href="{{ url('members/destroy/'.$id) }}">Delete User&nbsp;&nbsp; <i class="fa fa-close"></i></a>
                                        </li>
                                        @endrole
                                    </ul>
                                </div>
                            </div>                            
                        </div>
                        @endif
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop
@section('js')
<script>
    $(document).ready(function(){
        $('#award-pins-btn').click(function () {
            $('#award-pins-form').toggle('slow');
        });
    });
</script>
@stop