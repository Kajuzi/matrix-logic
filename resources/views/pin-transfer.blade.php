@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - PIN Transfer</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Transfer PIN to another member</h4>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open(array('url'=>url('pins/transfer'), 'method'=>'POST', 'class' => 'form-horizontal')) !!}
                                        <div class="form-group">
                                            <label for="recipient" class="col-sm-2 control-label">Transfer To</label>
                                            <div class="col-sm-10">                                                
                                                <div class="input-group m-b-sm">
                                                    <input class="form-control" id="recipient" placeholder="Recipient's username" type="text" name="recipient" required="required">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" id="check" type="button">Check</button>
                                                    </span>
                                                </div>
                                                <p class="recipient-block">&nbsp;</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="amount" class="col-sm-2 control-label">Number of PIN</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" id="amount" placeholder="0" type="text" name="amount" required="required">
                                                <p class="help-block">You have {{ $pins['available'] }} pins available</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="password" id="password" placeholder="Password" type="password" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="purpose" class="col-sm-2 control-label">Purpose</label>
                                            <div class="col-sm-10">
                                                {!! Form::textarea('purpose', '', array('required' => 'required', 'class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Transfer PIN</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop

@section('js')
<script type="text/javascript">
        $('#check').on('click', function(){
            $.get('{{ url('members/ajax-fullname') }}', { username : $('#recipient').val() })
                .done(function(fullname){
                    $('.recipient-block').html(fullname);
                })
        });
        var accepted = false;
        function showForm (id) {
            $(id).toggle(1000, 'swing');
        }

</script>
@stop