<!DOCTYPE html>
<html>
<head>
    <title>Ewallet History</title>
    <meta charset="UTF-8">
    <style>
		body {
			font-family: helvetica;
		}
		h2 {
			text-align: center;
		}
		th {
			background: whitesmoke;
			font-weight: bold;
		}

		.small-italics {
			font-size: 11px;
			font-style: italic;
		}

		.text-danger {
			color: red;
		}
    </style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:50%"><strong><img src="{{ asset('assets/images/logo.png') }}" height="45px" /></strong></td>
			<td style="width:50%; text-align: right">
			{{ \App\Models\Member::fullname(\Auth::id()) }}<br>
			{{ date('d-m-Y') }}
			</td>
		</tr>
	</tbody>
</table>
<h2>Approved eWallet Transactions</h2>

<table align="center" border="1" cellpadding="0" cellspacing="0" style="width:100%; border: 1px solid #ccc">
	<thead>
		<tr>
			<th scope="col">Date</th>
			<th scope="col">Amount</th>
			<th scope="col">Description</th>
			<th scope="col">From/To</th>
			<th scope="col">Balance</th>
		</tr>
	</thead>
	<tbody>
		@if(count($approved))
		@foreach($approved as $key => $row)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $row->amount }}</td>
			<td>{{ $row->purpose }}</td>
			<td>Placeholder_text</td>
			<td>{{ $row->sender_balance }}</td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="5">You do not have any approved transactions</td>
		</tr>
		@endif
	</tbody>
</table>
<h2>Declined eWallet Transactions</h2>

<table align="center" border="1" cellpadding="0" cellspacing="0" style="width:100%; border: 1px solid #ccc">
	<thead>
		<tr>
			<th scope="col">Date</th>
			<th scope="col">Amount</th>
			<th scope="col">Description</th>
			<th scope="col">From/To</th>
			<th scope="col">Balance</th>
		</tr>
	</thead>
	<tbody>
		@if(count($declined))
		@foreach($declined as $key => $row)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $row->amount }}</td>
			<td>{{ $row->purpose }}</td>
			<td>Placeholder_text</td>
			<td>{{ $row->sender_balance }}</td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="5">You do not have any declined transactions</td>
		</tr>
		@endif
	</tbody>
</table>
<h2>Pending eWallet Transactions</h2>

<table align="center" border="1" cellpadding="0" cellspacing="0" style="width:100%; border: 1px solid #ccc">
	<thead>
		<tr>
			<th scope="col">Date</th>
			<th scope="col">Amount</th>
			<th scope="col">Description</th>
			<th scope="col">From/To</th>
			<th scope="col">Balance</th>
		</tr>
	</thead>
	<tbody>
		@if(count($pending))
		@foreach($pending as $key => $row)
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $row->amount }}</td>
			<td>{{ $row->purpose }}</td>
			<td>Placeholder_text</td>
			<td>{{ $row->sender_balance }}</td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="5">Yo dou not have any pending transactions</td>
		</tr>
		@endif
	</tbody>
</table>

<p class="small-italics text-danger">Amounts in red indicate funds out of your account</p>

</body>
</html>