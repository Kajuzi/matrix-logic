<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Matrix Logic | Login</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Matrix Logic" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Kajuzi" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="{{ asset("assets/plugins/pace-master/themes/blue/pace-theme-flash.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/uniform/css/uniform.default.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/fontawesome/css/font-awesome.css") }}" rel="stylesheet" type="text/css">
        <!-- Theme Styles -->
        <link href="{{ asset("assets/css/modern.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/css/custom.css") }}" rel="stylesheet" type="text/css">

        <style type="text/css">
            .page-content{
                background: #B4D3EA;
            }
        </style>
        
        <script type="text/javascript">{{ asset("assets/plugins/3d-bold-navigation/js/modernizr.js") }}</script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-login">
        <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-3 center">
                            <div class="login-box">
                                <a href="{{ url('/') }}" class="logo-name text-lg text-center">Matrix Logic</a>
                                @if (Session::has('errors'))
                                    <div class="alert alert-error alert-danger">{{{ Session::get('errors')->first() }}}</div>
                                @else
                                    <p class="text-center m-t-md">Please login into your account.</p>
                                @endif

                                @if (Session::get('notice'))
                                    <div class="alert">{{{ Session::get('notice') }}}</div>
                                @endif
                                <form method="POST" action="{{ url('auth/login') }}">
                                {!! csrf_field() !!}
                                    <div class="form-group">
                                        <input name="email" type="email" class="form-control" tabindex="1" placeholder="Email"  value="{{{ Input::old('username') }}}"required>
                                    </div>

                                    <div class="form-group">
                                        <input name="password" type="password" class="form-control" tabindex="2" placeholder="Password" required>
                                    </div>

                                    <div class="checkbox">
                                        <label for="remember">
                                            <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Remember Me
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-success btn-block">Login</button>
                                    <!-- 
                                    <a href="{{ url('auth/reset_password') }}" class="display-block text-center m-t-md text-sm">Forgot Password?</a>
                                    <p class="text-center m-t-xs text-sm">Do not have an account?</p>
                                    <a href="{{ url('auth/register') }}" class="btn btn-default btn-block m-t-md">Create an account</a>
                                     -->
                                </form>

                                <p class="text-center m-t-xs text-sm">2015 &copy; Site by <a href="http://kajuzi.com">Kajuzi</a>.</p>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->    

        <!-- Javascripts -->
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-2.1.4.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/pace-master/pace.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery-blockui/jquery.blockui.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/uniform/jquery.uniform.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/js/modern.min.js") }}"></script>
        
    </body>
</html>