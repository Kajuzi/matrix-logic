<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Matrix Logic | Register A Member</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Matrix Logic" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Kajuzi" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="{{ asset("assets/plugins/pace-master/themes/blue/pace-theme-flash.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/uniform/css/uniform.default.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/fontawesome/css/font-awesome.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/line-icons/simple-line-icons.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/waves/waves.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/switchery/switchery.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/plugins/3d-bold-navigation/css/style.css") }}" rel="stylesheet" type="text/css">
        
        <!-- Theme Styles -->
        <link href="{{ asset("assets/css/modern.min.css") }}" rel="stylesheet" type="text/css">
        <link href="{{ asset("assets/css/custom.css") }}" rel="stylesheet" type="text/css">
        
        <script type="text/javascript">{{ asset("assets/plugins/3d-bold-navigation/js/modernizr.js") }}</script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-login">
        <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-3 center">
                            <div class="login-box">
                                <a href="{{ url('/') }}" class="logo-name text-lg text-center">Matrix Logic</a>
                                @if (Session::get('error'))
                                    <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
                                @else
                                    <p class="text-center m-t-md">Register New Member</p>
                                @endif

                                @if (Session::get('notice'))
                                    <div class="alert">{{{ Session::get('notice') }}}</div>
                                @endif
                                <form method="POST" action="{{ url('auth/register') }}">
                                {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label for="fname">First Name</label>
                                        <input class="form-control" type="text" name="fname" value="{{ old('fname') }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="lname">Last Name</label>
                                        <input class="form-control" type="text" name="lname" value="{{ old('lname') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input class="form-control" type="text" name="username" value="{{ old('username') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" type="email" name="email" value="{{ old('email') }}" required="required">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" name="password">
                                    </div>

                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input class="form-control" type="password" name="password_confirmation">
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Account Type</label>
                                        <div class="col-sm-8">
                                            <select class="form-control m-b-sm" name="role">
                                                <option value="3">Member</option>
                                                <option value="2">Manager</option>
                                                <option value="1">Administrator</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-success btn-block">Register New User</button>
                                    </div>
                                </form> 
                                <p class="text-center m-t-xs text-sm">2015 &copy; Site by <a href="http://kajuzi.com">Kajuzi</a>.</p>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
    

        <!-- Javascripts -->
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-2.1.4.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/pace-master/pace.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery-blockui/jquery.blockui.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/switchery/switchery.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/uniform/jquery.uniform.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/classie/classie.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/plugins/waves/waves.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("assets/js/modern.min.js") }}"></script>
        
    </body>
</html>