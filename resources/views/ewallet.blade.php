@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - eWallet History</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-3">
                        @include('includes.ewallet-sidebar')
                        </div>                        
                        <div class="col-md-9">
                        @include('includes.ewallet-forms')
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">eWallet Account History <i class="fa fa-"></i></h4>
                                </div>
                                <div class="panel-body">
                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a aria-expanded="true" href="#approved" role="tab" data-toggle="tab">Approved</a></li>
                                            <li role="presentation" class=""><a aria-expanded="false" href="#pending" role="tab" data-toggle="tab">Pending Approval</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="approved">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th align="center">Transaction ID</th>
                                                                <th align="center">Date</th>
                                                                <th align="center">Transaction<br>Ammount ({{ settings('currency_symbol') }})</th>
                                                                <th align="center">Balance ({{ settings('currency_symbol') }})</th>
                                                                <th align="center">Description</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($transactions['approved'] as $t)
                                                                <tr>
                                                                    <td>{{ $t->id }}</td>
                                                                    <th scope="row">{{ $t->ago }}</th>
                                                                    <td>{!! $t->amount !!}</td>
                                                                    <td>{{ $t->balance }}</td>
                                                                    <td>{{ $t->description }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>                                            
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade in" id="pending">
                                                <p>The transactions listed below are pending approval by admin</p>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th align="center">Transaction ID</th>
                                                                <th align="center">Date</th>
                                                                <th align="center">Transaction<br>Ammount ({{ settings('currency_symbol') }})</th>
                                                                <th align="center">Balance ({{ settings('currency_symbol') }})</th>
                                                                <th align="center">Description</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($transactions['pending'] as $p)
                                                                <tr>
                                                                    <td>{{ $p->id }}</td>
                                                                    <th scope="row">{{ $p->ago }}</th>
                                                                    <td>{!! $p->amount !!}</td>
                                                                    <td>{{ $p->balance }}</td>
                                                                    <td>{{ $p->description }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop
@section('css')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css">
@stop
@section('js')
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-timepicker.js') }}"></script>
    <script>
        $('#check-recipient-info').on('click', function(){
            $.get('{{ url('members/ajax-fullname') }}', { username : $('#transfer-username').val() })
                .done(function(fullname){
                    $('#recipient-info').html(fullname);
                })
        });
        var accepted = false;
        function showForm (id) {
            $(id).toggle(1000, 'swing');
        }


        function checkfile () {
            var file = $('#proof-file');
            filename = file.val();
            var ext = filename.match(/\.([^\.]+)$/)[1];
            var lcext = ext.toLowerCase();
            switch(ext)
            {
                case 'jpg':
                case 'jpeg':
                case 'pdf':
                case 'png':
                case 'tif':
                    accepted = true;
                    break;
                default:
                    $('#proof-info').html('Filetype not acceptable!');
                    $('#proof-info').addClass('text-danger');
                    accepted = false;
                    file.val('');
            }
        };

        function checkAffordable () {
            var info = $('#quantity-info')
            var quantity = $('#quantity');
            var a = quantity.val();
            info.removeClass('text-danger');
            if(!$.isNumeric(a)){
                info.html('Please enter a number');
                info.addClass('text-danger');
                return false;
            }
            if(a > {{ $balance/Config::get('settings.pin_price') }}){
                info.html('You can\'t afford more than {{ $balance/Config::get('settings.pin_price') }} pins');
                info.addClass('text-danger');
                quantity.val({{ $balance/Config::get('settings.pin_price') }});
                return false;                
            }
        };
    </script>
@stop