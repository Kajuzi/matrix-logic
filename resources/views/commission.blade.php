@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Commissions</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Your Commissions</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-primary">
                                        <tbody>
                                            <tr>
                                                <td class="link">Downline Contributions</td>
                                                <td align="right"><strong class="green">{{ settings('currency_symbol') }}{{ number_format($downline_contributions) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="link">Earnings</td>
                                                <td align="right"><strong class="green">{{ settings('currency_symbol') }}{{ number_format($total) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="link">Pay</td>
                                                <td align="right"><strong class="text-danger">{{ settings('currency_symbol') }}{{ number_format($pay) }}</strong></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <strong class="total-amount pull-right">Total: <span class="green">{{ settings('currency_symbol') }}{{ number_format($sum) }}</span></strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop