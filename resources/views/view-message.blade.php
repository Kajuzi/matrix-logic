@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Profile</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <div class="row mailbox-header">
                                <div class="col-md-2">
                                    <a href="{{ url('mailbox/compose') }}" class="btn btn-success btn-block">Compose</a>
                                </div>
                                <div class="col-md-6">
                                    <h2>Inbox</h2>
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                               </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <ul class="list-unstyled mailbox-nav">
                                <li{!! ($folder == 'inbox')?' class="active"':'' !!}><a href="{{ url('mailbox/inbox') }}"><i class="fa fa-inbox"></i>Inbox @if($unread) <span class="badge badge-success pull-right">{{ $unread }}@endif </span></a></li>
                                <li{!! ($folder == 'sent')?' class="active"':'' !!}><a href="{{ url('mailbox/sent') }}"><i class="fa fa-sign-out"></i>Sent</a></li>
                                <li{!! ($folder == 'trash')?' class="active"':'' !!}><a href="{{ url('mailbox/trash') }}"><i class="fa fa-trash"></i>Trash</a></li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="panel panel-white">
                                <div class="panel-body mailbox-content">
                                    <div class="message-header">
                                        <h3><span>Subject:</span> {{ $message->title }} </h3>
                                        <p class="message-date">{{ $message->when }}</p>
                                    </div>
                                    <div class="message-sender">
                                        <img src="assets/images/avatar2.png" alt="">
                                        <p>From: <strong>{{ $message->sender }}</strong> <span>{{ $message->username }}</span></p>
                                    </div>
                                    <div class="message-content">
                                        {!! $message->body !!}
                                    </div>
                                    <div class="message-options pull-right">
                                        <a href="{{ url('mailbox/compose') }}" class="btn btn-default"><i class="fa fa-reply m-r-xs"></i>Reply</a> 
                                        <!-- <a href="#" class="btn btn-default"><i class="fa fa-print m-r-xs"></i>Print</a> 
                                        <a href="#" class="btn btn-default"><i class="fa fa-trash m-r-xs"></i>Delete</a>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Main Wrapper -->
@stop