@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Downline</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Your Downline</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-primary">
                                        <thead>
                                            <tr class="tac">
                                                <th align="center">#</th>
                                                <th align="center">Username</th>
                                                <th align="center">Full Name</th>
                                                <th align="center">Joined</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($downline as $key => $d)
                                            <tr>
                                                <td align="center">{{ $key + 1 }}</td>
                                                <td align="center">{{ $d['username'] }}</td>
                                                <td>
                                                    {{ $d['fname'].' '.$d['lname']}}
                                                </td>
                                                <td align="center">{{ \App\Models\Handy::ago($d['created_at']) }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop