@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - PIN History</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">PIN History</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="text-center">
                                                    <th align="center">Date</th>
                                                    <th align="center">Quantity</th>
                                                    <th align="center">Description</th>
                                                    <th align="center">Sender/Recipient</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($transfers as $t)
                                                <tr>
                                                    <th scope="row">{{ $t->when }}</th>
                                                    <td>{{ $t->amount }}</td>
                                                    <td>{{ $t->purpose }}</td>
                                                    <td>{{ $t->other_party }}</td>
                                                </tr>
                                            @endforeach()
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop