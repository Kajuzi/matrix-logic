@extends('layouts.master-layout-1')

@section('content')
                <div class="page-title">
                    <div class="container">
                        <div class="col-md-6">
                            <h3>{{ $fname }} {{ $lname }} - Announcements</h3>    
                        </div>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Announcements</h4>
                                </div>
                                <div class="panel-body">
                                    @foreach($announcements as $a)
                                    <div>
                                        <h4>{{ ago($a->publish_at) }} : <span class="{{ $a->class }}">{{ $a->title }}</span></h4>
                                        <p>{{ $a->body }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>                            
                        </div>
                    </div> <!-- /Row -->
                </div><!-- Main Wrapper -->
@stop
@section('js')

@stop
@section('css')
@stop