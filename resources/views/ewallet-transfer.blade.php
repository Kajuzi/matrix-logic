@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - eWallet History</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-3">
                        @include('includes.ewallet-sidebar')
                        </div>
                        <div class="col-md-9">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">eWallet Account History <i class="fa fa-clock"></i></h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="text-center">
                                                    <th align="center">Transaction ID</th>
                                                    <th align="center">Date</th>
                                                    <th align="center">Transaction<br>Ammount ({{ settings('currency_symbol') }})</th>
                                                    <th align="center">Balance ({{ settings('currency_symbol') }})</th>
                                                    <th align="center">Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($transactions as $t)
                                                    <tr>
                                                        <td>{{ $t->id }}</td>
                                                        <th scope="row">{{ $t->ago }}</th>
                                                        <td>{!! $t->amount !!}</td>
                                                        <td>{{ $t->balance }}</td>
                                                        <td>{{ $t->description }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop