@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Profile</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <div class="row mailbox-header">
                                <div class="col-md-2">
                                    <a href="{{ url('mailbox/compose') }}" class="btn btn-success btn-block">Compose</a>
                                </div>
                                <div class="col-md-6">
                                    <h2>{{ ucfirst($folder) }}</h2>
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                               </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <ul class="list-unstyled mailbox-nav">
                                <li{!! ($folder == 'inbox')?' class="active"':'' !!}><a href="{{ url('mailbox/inbox') }}"><i class="fa fa-inbox"></i>Inbox @if($unread) <span class="badge badge-success pull-right">{{ $unread }}@endif </span></a></li>
                                <li{!! ($folder == 'sent')?' class="active"':'' !!}><a href="{{ url('mailbox/sent') }}"><i class="fa fa-sign-out"></i>Sent</a></li>
                                <li{!! ($folder == 'trash')?' class="active"':'' !!}><a href="{{ url('mailbox/trash') }}"><i class="fa fa-trash"></i>Trash</a></li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="panel panel-white">
                            <div class="panel-body mailbox-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="1" class="hidden-xs">
                                            <span><div class="checker"><span><input class="check-mail-all" type="checkbox"></span></div></span>
                                        </th>
                                        <th class="text-right" colspan="5">
                                            <span class="text-muted m-r-sm">Showing {{ count($messages) }} of {{ count($messages) }} </span>
                                            <a data-original-title="Refresh" class="btn btn-default m-r-sm" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-refresh"></i></a>
                                            <div style="display: none;" class="btn-group m-r-sm mail-hidden-options">                                                
                                            </div>
                                            <div style="display: none;" class="btn-group m-r-sm mail-hidden-options">
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-default"><i class="fa fa-angle-left"></i></a>
                                                <a class="btn btn-default"><i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!count($messages))
                                    <tr>
                                        <td class="text-info">You have no messages in the {{ $folder }} folder</td>
                                    </tr>
                                    @else
                                    @foreach($messages as $msg)
                                    <tr class="{{ ($msg->read)?'read':'unread' }}">
                                        <td class="hidden-xs">
                                            <span><div class="checker"><span><input class="checkbox-mail" type="checkbox"></span></div></span>
                                        </td>
                                        <td class="hidden-xs">
                                            @if($folder == 'inbox' && $msg->starred == 1)<i class="fa fa-dot-star icon-state-danger"></i>@endif
                                        </td>
                                        <td class="hidden-xs">
                                            {{ $msg->sender }}
                                        </td>
                                        <td>
                                            <a href="{{ url('mailbox/view/'.$msg->id) }}">{{ $msg->title }}</a>
                                        </td>
                                        <td>
                                            {{ date('d M Y', strtotime($msg->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ url('mailbox/delete/'.$msg->id) }}"><i class="fa fa-trash icon-state-danger"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Main Wrapper -->
@stop