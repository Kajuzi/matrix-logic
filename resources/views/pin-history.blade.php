@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - PIN History</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">PIN History</h4>
                                </div>
                                <div class="panel-body">
                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a aria-expanded="true" href="#tab1" role="tab" data-toggle="tab">All</a></li>
                                            <li class="" role="presentation"><a aria-expanded="false" href="#tab2" role="tab" data-toggle="tab">Pending</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade active in" id="tab1">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th align="center">Date</th>
                                                                <th align="center">Sender/Recipient</th>
                                                                <th align="center">Amount</th>
                                                                <th align="center">Description</th>
                                                                <th align="center">Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($history)
                                                            @foreach($history as $k => $row)
                                                                <tr>
                                                                    <th scope="row">{{ $row->when }}</th>
                                                                    <td>{{ $row->other_party }}</td>
                                                                    <td{{ ($row->amount<0)?' class=text-danger':'' }}>{{ $row->amount }}</td>
                                                                    <td>{{ $row->purpose }}</td>
                                                                    <td>{{ $row->approved }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @else 
                                                            <tr>
                                                                <td colspan="4">You do not have any pin transactions</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>         
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="tab2">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th align="center">Date</th>
                                                                <th align="center">Sender/Recipient</th>
                                                                <th align="center">Amount</th>
                                                                <th align="center">Description</th>
                                                                <th align="center">Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($pending)
                                                            @foreach($pending as $k => $row)
                                                                <tr>
                                                                    <th scope="row">{{ $row->when }}</th>
                                                                    <td>{{ $row->other_party }}</td>
                                                                    <td{{ ($row->amount<0)?' class=text-danger':'' }}>{{ $row->amount }}</td>
                                                                    <td>{{ $row->purpose }}</td>
                                                                    <td>{{ $row->approved }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @else 
                                                            <tr>
                                                                <td colspan="4">You do not have any pending pin transactions</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop