@extends('layouts.master-layout-1')
@section('content')
            <div class="page-inner">
                <!-- <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">X-editable</li>
                    </ol>
                </div> -->
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - PIN History</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Pending Pin Purchases</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th align="center">Date</th>
                                                <th align="center">Recipient</th>
                                                <th align="center">Amount</th>
                                                <th align="center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if($requests)
                                            @foreach($requests as $k => $row)
                                                <tr id="row_{{ $row->id }}">
                                                    <th scope="row">{{ $row->when }}</th>
                                                    <td>{{ $row->other_party }}</td>
                                                    <td{{ ($row->amount<0)?' class=text-danger':'' }}>{{ $row->amount }}</td>
                                                    <td><a href="#" onclick="return accept({{ $row->id }}, {{ $row->amount }})">Accept</a> | <a href="#" class="text-danger" onclick="return decline({{ $row->id }}, {{ $row->amount }})">Decline</a></td>
                                                </tr>
                                            @endforeach
                                        @else 
                                            <tr>
                                                <td colspan="4">You do not have anyone requesting to buy pins</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop

@section('js')
<script>
    function accept (tid, quantity) {
        var id = tid;
        var qty = Math.abs(parseInt(quantity));
        $.get('{{ url('pins/approve-purchase') }}/' + id + '/' + qty, function (data) {
            if(data == 'success'){
                $('#row_'+id).toggle('slow');
            }else{
                alert("Request Not Processed");
            }
        })

       return false; 
    }
    function decline (tid, quantity) {
        var id = tid;
        var qty = Math.abs(parseInt(quantity));
        $.get('{{ url('pins/decline-purchase') }}/' + id + '/' + qty, function (data) {
            if(data == 'success'){
                $('#row_'+id).toggle('slow');
            }else{
                alert("Request Not Processed");
            }
        })

       return false; 
    }
</script>
@stop