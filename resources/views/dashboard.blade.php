@extends('layouts.master-layout-1')

@section('content')
                <!-- 
                <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
                -->
                <div class="page-title">
                    <div class="container">
                        <div class="col-md-6">
                            <h3>{{ $fname }} {{ $lname }} - Dashboard</h3>    
                        </div>
                        @permission('register-member')
                            <div class="col-md-offset-2 col-md-4">
                                <a href="{{ url('members/register') }}" class="btn btn-danger btn-rounded btn-lg reg-btn"><span aria-hidden="true" class="icon-user-follow"></span>  REGISTER A MEMBER</a>
                            </div>
                        @endpermission
                        @role('member')
                            <div class="col-md-offset-2 col-md-4">
                                <a href="{{ url('pins/buy') }}" class="btn btn-danger btn-rounded btn-lg reg-btn"><span aria-hidden="true" class="icon-energy"></span>  Request To Buy Pins</a>
                            </div>
                        @endrole
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Account Summary</h4>
                                </div>
                                <div class="panel-body">
                                    <p class="col-md-6">
                                    <span class="col-sm-5">Full Name :</span> <span class="col-sm-7">{{ $fname }} {{ $lname }}</span>
                                    <span class="col-sm-5">Primary Bank :</span> <span class="col-sm-7">{{ $title }}</span>
                                    <span class="col-sm-5">Primary Account No. :</span> <span class="col-sm-7">{{ $account_num }}</span>
                                    </p>
                                    <p class="col-md-6">
                                    <span class="col-sm-5">No. of PIN :</span> <span class="col-sm-7">{{ $pin['all'] }} ({{ $pin['available'] }} available)</span>
                                    <span class="col-sm-5">Downline :</span> <span class="col-sm-7">{{ $downline }}</span>
                                    <span class="col-sm-5">eWallet Balance :</span> <span class="col-sm-7">{{ settings('currency_symbol') }}{{ number_format($ewbalance) }}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Announcements</h4>
                                </div>
                                <div class="panel-body">
                                    @foreach($announcements as $a)
                                    <div>
                                        <h4>{{ ago($a->publish_at) }} : <span class="{{ $a->class }}">{{ $a->title }}</span></h4>
                                        <p>{{ $a->body }}</p>
                                    </div>
                                    @endforeach
                                    <a href="{{ url('announcements') }}" class="btn btn-primary pull-right">See All</a>
                                </div>
                            </div>                            
                        </div>
                        @role('admin')
                        <div class="col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">New Members</h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>Join Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($new_members as $nm)
                                            <tr>
                                                <td>{{ $nm->username }}</td>
                                                <td>{{ $nm->created_at }}</td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                        @endrole
                    </div> <!-- /Row -->
                </div><!-- Main Wrapper -->
@stop
@section('js')

@stop
@section('css')
@stop