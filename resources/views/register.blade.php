@extends('layouts.master-layout-1')
@section('content')
            <?php $countries = ["" => "Select Country", "Afghanistan"=>"Afghanistan", "Albania"=>"Albania", "Algeria"=>"Algeria", "Algeria"=>"Algeria", "Algeria"=>"Algeria", "Antigua and Barbuda"=>"Antigua and Barbuda", "Argentina"=>"Argentina", "Armenia"=>"Armenia", "Australia"=>"Australia", "Austria"=>"Austria", "Azerbaijan"=>"Azerbaijan", "Bahamas"=>"Bahamas", "Bahrain"=>"Bahrain", "Bangladesh"=>"Bangladesh", "Barbados"=>"Barbados", "Belarus"=>"Belarus", "Belgium"=>"Belgium", "Belize"=>"Belize", "Benin"=>"Benin", "Bhutan"=>"Bhutan", "Bolivia"=>"Bolivia", "Bosnia and Herzegovina"=>"Bosnia and Herzegovina", "Botswana"=>"Botswana", "Brazil"=>"Brazil", "Brunei"=>"Brunei", "Bulgaria"=>"Bulgaria", "Burkina Faso"=>"Burkina Faso", "Burundi"=>"Burundi", "Cambodia"=>"Cambodia", "Cameroon"=>"Cameroon", "Canada"=>"Canada", "Cape Verde"=>"Cape Verde", "Central African Republic"=>"Central African Republic", "Chad"=>"Chad", "Chile"=>"Chile", "China"=>"China", "Colombi"=>"Colombi", "Comoros"=>"Comoros", "Congo (Brazzaville)"=>"Congo (Brazzaville)", "Congo"=>"Congo", "Costa Rica"=>"Costa Rica", "Cote d'Ivoire"=>"Cote d'Ivoire", "Croatia"=>"Croatia", "Cuba"=>"Cuba", "Cyprus"=>"Cyprus", "Czech Republic"=>"Czech Republic", "Denmark"=>"Denmark", "Djibouti"=>"Djibouti", "Dominica"=>"Dominica", "Dominican Republic"=>"Dominican Republic", "East Timor (Timor Timur)"=>"East Timor (Timor Timur)", "Ecuador"=>"Ecuador", "Egypt"=>"Egypt", "El Salvador"=>"El Salvador", "Equatorial Guinea"=>"Equatorial Guinea", "Eritrea"=>"Eritrea", "Estonia"=>"Estonia", "Ethiopia"=>"Ethiopia", "Fiji"=>"Fiji", "Finland"=>"Finland", "France"=>"France", "Gabon"=>"Gabon", "Gambia, The"=>"Gambia, The", "Georgia"=>"Georgia", "Germany"=>"Germany", "Ghana"=>"Ghana", "Greece"=>"Greece", "Grenada"=>"Grenada", "Guatemala"=>"Guatemala", "Guinea"=>"Guinea", "Guinea-Bissau"=>"Guinea-Bissau", "Guyana"=>"Guyana", "Haiti"=>"Haiti", "Honduras"=>"Honduras", "Hungary"=>"Hungary", "Iceland"=>"Iceland", "India"=>"India", "Indonesia"=>"Indonesia", "Iran"=>"Iran", "Iraq"=>"Iraq", "Ireland"=>"Ireland", "Israel"=>"Israel", "Italy"=>"Italy", "Jamaica"=>"Jamaica", "Japan"=>"Japan", "Jordan"=>"Jordan", "Kazakhstan"=>"Kazakhstan", "Kenya"=>"Kenya", "Kiribati"=>"Kiribati", "Korea, North"=>"Korea, North", "Korea, South"=>"Korea, South", "Kuwait"=>"Kuwait", "Kyrgyzstan"=>"Kyrgyzstan", "Laos"=>"Laos", "Latvia"=>"Latvia", "Lebanon"=>"Lebanon", "Lesotho"=>"Lesotho", "Liberia"=>"Liberia", "Libya"=>"Libya", "Liechtenstein"=>"Liechtenstein", "Lithuania"=>"Lithuania", "Luxembourg"=>"Luxembourg", "Macedonia"=>"Macedonia", "Madagascar"=>"Madagascar", "Malawi"=>"Malawi", "Malaysia"=>"Malaysia", "Maldives"=>"Maldives", "Mali"=>"Mali", "Malta"=>"Malta", "Marshall Islands"=>"Marshall Islands", "Mauritania"=>"Mauritania", "Mauritius"=>"Mauritius", "Mexico"=>"Mexico", "Micronesia"=>"Micronesia", "Moldova"=>"Moldova", "Monaco"=>"Monaco", "Mongolia"=>"Mongolia", "Morocco"=>"Morocco", "Mozambique"=>"Mozambique", "Myanmar"=>"Myanmar", "Namibia"=>"Namibia", "Nauru"=>"Nauru", "Nepa"=>"Nepa", "Netherlands"=>"Netherlands", "New Zealand"=>"New Zealand", "Nicaragua"=>"Nicaragua", "Niger"=>"Niger", "Nigeria"=>"Nigeria", "Norway"=>"Norway", "Oman"=>"Oman", "Pakistan"=>"Pakistan", "Palau"=>"Palau", "Panama"=>"Panama", "Papua New Guinea"=>"Papua New Guinea", "Paraguay"=>"Paraguay", "Peru"=>"Peru", "Philippines"=>"Philippines", "Poland"=>"Poland", "Portugal"=>"Portugal", "Qatar"=>"Qatar", "Romania"=>"Romania", "Russia"=>"Russia", "Rwanda"=>"Rwanda", "Saint Kitts and Nevis"=>"Saint Kitts and Nevis", "Saint Lucia"=>"Saint Lucia", "Saint Vincent"=>"Saint Vincent", "Samoa"=>"Samoa", "San Marino"=>"San Marino", "Sao Tome and Principe"=>"Sao Tome and Principe", "Saudi Arabia"=>"Saudi Arabia", "Senegal"=>"Senegal", "Serbia and Montenegro"=>"Serbia and Montenegro", "Seychelles"=>"Seychelles", "Sierra Leone"=>"Sierra Leone", "Singapore"=>"Singapore", "Slovakia"=>"Slovakia", "Slovenia"=>"Slovenia", "Solomon Islands"=>"Solomon Islands", "Somalia"=>"Somalia", "South Africa"=>"South Africa", "Spain"=>"Spain", "Sri Lanka"=>"Sri Lanka", "Sudan"=>"Sudan", "Suriname"=>"Suriname", "Swaziland"=>"Swaziland", "Sweden"=>"Sweden", "Switzerland"=>"Switzerland", "Syria"=>"Syria", "Taiwan"=>"Taiwan", "Tajikistan"=>"Tajikistan", "Tanzania"=>"Tanzania", "Thailand"=>"Thailand", "Togo"=>"Togo", "Tonga"=>"Tonga", "Trinidad and Tobago"=>"Trinidad and Tobago", "Tunisia"=>"Tunisia", "Turkey"=>"Turkey", "Turkmenistan"=>"Turkmenistan", "Tuvalu"=>"Tuvalu", "Uganda"=>"Uganda", "Ukraine"=>"Ukraine", "United Arab Emirates"=>"United Arab Emirates", "United Kingdom"=>"United Kingdom", "United States"=>"United States", "Uruguay"=>"Uruguay", "Uzbekistan"=>"Uzbekistan", "Vanuatu"=>"Vanuatu", "Vatican City"=>"Vatican City", "Venezuela"=>"Venezuela", "Vietnam"=>"Vietnam", "Yemen"=>"Yemen", "Zambia"=>"Zambia", "Zimbabwe"=>"Zimbabwe"] ?>
            <div class="page-inner">
                <div class="page-title">
                    <div class="container">
                        <h3>{{ $fname }} {{ $lname }} - Profile</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Register A User</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-9 col-md-offset-3">
                                        @if(Session::has('errors'))
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                Oh snap! Change a few things and try submitting again.
                                            </div>
                                        @endif
                                    </div>
                                    {!! Form::open(['class'=>"form-horizontal", 'onsubmit' => 'return valid8()']) !!}
                                        <div class="form-group">
                                            <label for="fname" class="col-md-3 control-label">First Name</label>
                                            <div class="col-md-9">
                                                {!! Form::text('fname', null, ["class"=>"form-control", "id"=>"fname", "placeholder"=>"New Member's First Name", "required"=>"required"]) !!}
                                                {!! $errors->first('fname', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lname" class="col-md-3 control-label">Last Name</label>
                                            <div class="col-md-9">
                                                
                                                {!! Form::text('lname', null, ["class"=>"form-control", "id"=>"lname", "placeholder"=>"New Member's Last Name", "required"=>"required"]) !!}
                                                {!! $errors->first('lname', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                {!! Form::email('email', null, ["class"=>"form-control", "id"=>"email", "placeholder"=>"New Member's Email", "required"=>"required"]) !!}
                                                {!! $errors->first('email', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="username" class="col-md-3 control-label">Username</label>
                                            <div class="col-md-9">
                                                {!! Form::text('username', null, ["class"=>"form-control", "id"=>"username", "placeholder"=>"New Member's Userame", "required"=>"required"]) !!}
                                                {!! $errors->first('username', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone" class="col-md-3 control-label">Phone Number</label>
                                            <div class="col-md-9">
                                                {!! Form::text('phone', null, ["class"=>"form-control", "id"=>"phone", "placeholder"=>"New Member's Phone", "required"=>"required"]) !!}
                                                {!! $errors->first('phone', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-md-3 control-label">Password</label>
                                            <div class="col-md-9">
                                                {!! Form::password('password', ["class"=>"form-control", "id"=>"password", "placeholder"=>"Choose a password", "required"=>"required"]) !!}
                                                {!! $errors->first('password', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation" class="col-md-3 control-label">Confirm Password</label>
                                            <div class="col-md-9">
                                                {!! Form::password('password_confirmation', ["class"=>"form-control", "id"=>"password_confirmation", "placeholder"=>"Retype the password", "required"=>"required"]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="country" class="col-md-3 control-label">Country</label>
                                            <div class="col-md-9">
                                                {!! Form::select('country', $countries, null, ['class' => 'form-control', 'id' => 'country']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-md-offset-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <div class="checker"><span class="checked"><input name="tncs" id="tncs" type="checkbox" onchange="return tncso()"></span></div> I fully understand and agree with the <a href="{{ url('tncs') }}">Terms &amp; Conditions</a>
                                                    </label>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-md-offset-3">
                                                <div class="checkbox">
                                                    <label>
                                                        Please note that after this registration one pin will be deducted and you will be left with {{ \App\Models\Pin::countAvailable(Auth::user()->id) - 1 }} pins
                                                    </label>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-9">
                                                {!! Form::submit('Register',['class' => 'btn disabled', 'id' => 'submit-btn']) !!}
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
@stop

@section('js')
<script type="text/javascript">
    function tncso() {
        if($('#tncs').is(':checked')){
            $('#submit-btn').removeClass('disabled');
            $('#submit-btn').addClass('btn-success');
        }else{
            $('#submit-btn').addClass('disabled');
            $('#submit-btn').removeClass('btn-success');
        }
    }

    function valid8(){
        if(!$('#tncs').is(':checked')){
            alert('You need to agree with the terms and conditions');
            return false;
        }
        if($('#country').val() == ''){
            alert('Please select country');
            return false;
        }
        return true;
    }
</script>
@stop