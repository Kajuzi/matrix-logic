@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-8 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Transactions pending approval</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Sender</th>
                                                    <th>Recipient</th>
                                                    <th>Amount</th>
                                                    <th>Purpose</th>
                                                    <th>Date</th>
                                                    <th>Proof</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!$pending_trans)
                                            <tr>
                                                <td colspan="8">There are no transactions pending approval</td>
                                            </tr>
                                            @else
                                            @foreach($pending_trans as $key => $t)
                                                <tr>
                                                    <th scope="row">{{ $key + 1 }}</th>
                                                    <td>{{ $t->sender->username }}</td>
                                                    <td>{{ $t->recipient->username }}</td>
                                                    <td>{{ $t->amount }}</td>
                                                    <td>{{ $t->description }}</td>
                                                    <td>{{ $t->ago }}</td>
                                                    <td>
                                                        @if($t->proof)
                                                        <a href="{{ asset('uploads/'.$t->proof) }}" target="_blank">Proof</a>
                                                        @else
                                                        &nbsp;
                                                        @endif
                                                    </td>
                                                    <td><a href="{{ url('ewallet/approve/'.$t->id) }}"><span id="{{ $t->id }}" class="approve">Approve</span></a> | <a href="{{ url('ewallet/decline/'.$t->id) }}">Decline</a></td>
                                                </tr>                                                
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Newest Registered Members</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Fullname</th>
                                                    <th>Manager</th>
                                                    <th>Country</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!$newbies)
                                            <tr>
                                                <td colspan="8">There are no new members on the site</td>
                                            </tr>
                                            @else
                                                @foreach($newbies as $key => $n)
                                                    <tr>
                                                        <th scope="row">{{ $key + 1 }}</th>
                                                        <td><a href="{{ url('members/profile/'.$n->id) }}">{{ $n->username }}</a></td>
                                                        <td><a href="{{ url('members/profile/'.$n->id) }}">{{ $n->fullname }}</a></td>                                                        
                                                        <td>
                                                            @if($n->manager_id)
                                                            <a href="{{ url('members/profile/'.$n->manager_id) }}">{{ $n->manager }}</a>
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td>{{ $n->country }}</td>
                                                        <td>{{ $n->when }}</td>
                                                        
                                                    </tr>                                                
                                                @endforeach
                                                <tr><td colspan="6" class="text-center"><a href="{{ url('admin/users') }}">See All</a></td>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <div class="panel-title">Post Announcement</div>
                                </div>
                                <div class="panel-body">
                                    <div class="post" id="announce">
                                        {!! Form::open(['onsubmit'=>'return announce()']) !!}
                                            <div class="form-group">
                                                {!! Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Title', 'id'=>'announcement-title']) !!}
                                            </div>
                                            <div class="form-group">                                            
                                                <textarea name="body" id="announcement-body" class="form-control" placeholder="Post an announcement" rows="4=6"></textarea>
                                            </div>
                                            {!! Form::hidden('publish_at', date('Y-m-d H:i:s'), ['id'=>'publish_at']) !!}
                                            {!! Form::hidden('expires_at', date('Y-m-d H:i:s', time() + (7 * 24 * 60 * 60)), ['id'=>'expires_at'] ) !!}
                                            <div class="post-options">
                                                <span class="pull-left"><a href="{{ url('admin/announce') }}">More Options</a></span> {!! Form::submit('Post',['class'=>"btn btn-default pull-right"]) !!}
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <div class="panel-title">Newest Members</div>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        @foreach($summary['new_members'] as $nm)
                                        <li>
                                            <a href="{{ url('members/profile/'.$nm->id) }}">{{ $nm->username }}</a>
                                        </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
    <script>
        function announce () {
            var title = $('#announcement-title').val();
            var body = $('#announcement-body').val();
            var expires_at = $('input[name=expires_at]').val();
            var publish_at = $('input[name=publish_at]').val();
            var token = $('input[name=_token]').val();
            var announce = $('#announce');
            var parameters = { title : title, body : body, _token : token, expires_at: expires_at, publish_at: publish_at};

            $.post("{{ url('announcements') }}", parameters )
            .done(function(data) {
                announce.html('<h3 class="text-info">'+title+'</h3><p>'+body+'</p>');
            })
            .fail(function(data){
                announce.html('There has been an error. Please contact support');
            });
            return false;
        } 
    </script>
@stop