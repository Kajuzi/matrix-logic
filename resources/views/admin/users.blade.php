@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">All Registered Members</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Fullname</th>
                                                    <th>Manager</th>
                                                    <th>Country</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!$all_members)
                                            <tr>
                                                <td colspan="8">The website has no members</td>
                                            </tr>
                                            @else
                                                @foreach($all_members as $key => $m)
                                                    <tr>
                                                        <th scope="row">{{ $key + 1 }}</th>
                                                        <td><a href="{{ url('members/profile/'.$m->id) }}">{{ $m->username }}</a></td>
                                                        <td><a href="{{ url('members/profile/'.$m->id) }}">{{ $m->fullname }}</a></td>                                                        
                                                        <td>
                                                            @if($m->manager_id)
                                                            <a href="{{ url('members/profile/'.$m->manager_id) }}">{{ $m->manager }}</a>
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td>{{ $m->country }}</td>
                                                        <td>{{ $m->when }}</td>
                                                        <td>
                                                            @if($m->active)
                                                            <a href="{{ url('members/deactivate/'.$m->id) }}">Deactivate</a>
                                                            @else
                                                            <a href="{{ url('members/activate/'.$m->id) }}">Activate</a>
                                                            @endif
                                                        </td>
                                                        
                                                    </tr>                                                
                                                @endforeach
                                                <tr><td colspan="6" class="text-center"><a href="{{ url('admin/users') }}">See All</a></td>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
    <script>
        function announce () {
            var title = $('#announcement-title').val();
            var body = $('#announcement-body').val();
            var token = $('input[name=_token]').val();
            var announce = $('#announce');
            var parameters = { title : title, body : body, _token : token};

            $.post("announcements", parameters )
            .done(function(data) {
                announce.html('<h3 class="text-info">'+title+'</h3><p>'+body+'</p>');
            })
            .fail(function(data){
                announce.html('There has been an error. Please contact support');
            });
            return false;
        } 
    </script>
@stop