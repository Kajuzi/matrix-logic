<?php  
    $unread     = \App\Models\Mailbox::countUnread(Auth::id());
    $inbox      = \App\Models\Mailbox::inbox(Auth::id());
    $alerts     = alerts();
    $countAlerts= \App\Models\Alert::countUnread();
?>
<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Admin | Matrix Logic </title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Matrix Logic" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Kajuzi" />
        
        <!-- Styles -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'> -->
        <link href="{{ asset('assets/admin/plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/admin/plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/admin/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/admin/plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/admin/plugins/offcanvasmenueffects/css/menu_cornerbox.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/admin/plugins/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/admin/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/admin/plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/admin/plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">  
        
@yield('css')
        <!-- Theme Styles -->
        <link href="{{ asset('assets/admin/css/modern.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/admin/css/themes/green.css') }}" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet" type="text/css"/>
        
        <script src="{{ asset('assets/admin/plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>
        <div class="menu-wrap">
            <nav class="profile-menu">
                <div class="profile"><i class="fa fa-user"></i><span>{{ \Auth::user()->fname.' '.\Auth::user()->lname }}</span></div>
                <div class="profile-menu-list">
                    <a href="#"><i class="fa fa-star"></i><span>Favorites</span></a>
                    <a href="#"><i class="fa fa-bell"></i><span>Alerts</span></a>
                    <a href="#"><i class="fa fa-envelope"></i><span>Messages</span></a>
                    <a href="#"><i class="fa fa-comment"></i><span>Comments</span></a>
                </div>
            </nav>
            <button class="close-button" id="close-button">Close Menu</button>
        </div>
        <form class="search-form" action="#" method="GET">
            <div class="input-group">
                <input type="text" name="search" class="form-control search-input" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
                </span>
            </div><!-- Input Group -->
        </form><!-- Search Form -->
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ url('admin') }}" class="logo-text"><span>Matrix Admin</span></a>
                    </div><!-- Logo Box -->
                    <div class="search-button">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left">
                                <li>		
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-envelope"></i>@if($unread)<span class="badge badge-success pull-right">{{ $unread }}</span>@endif</a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have {{ ($unread)?$unread:'no' }} new  message{{ ($unread>1)?'s':'' }}!</p></li>
                                        @if($unread)
                                        <li class="dropdown-menu-list slimscroll messages">
                                            @foreach($inbox as $m)
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="#">
                                                        <div class="msg-img"><i class="fa fa-envelope icon-primary"></i><!-- <img class="img-circle" src="{{ asset('assets/images/avatar2.png') }}" alt=""> --></div>
                                                        <p class="msg-name">{{ \App\User::find($m->sender_id)->username }}</p>
                                                        <p class="msg-text">{{ $m->title }}</p>
                                                        <p class="msg-time">{{ \App\Models\Handy::ago($m->created_at) }}</p>
                                                    </a>
                                                </li>
                                            </ul>
                                            @endforeach
                                        </li>
                                        @endif
                                        <li class="drop-all"><a href="{{ url('mailbox/inbox') }}" class="text-center">All Messages</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-bell"></i>@if($countAlerts)<span id="countAlerts" class="badge badge-success pull-right">{{ $countAlerts }}</span>@endif</a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have {{ ($countAlerts)?$countAlerts:'no' }} new alert{{ ($countAlerts==1)?'':'s' }} !</p></li>
                                        @if($countAlerts)
                                        <li class="dropdown-menu-list slimscroll tasks">
                                            <ul class="list-unstyled">
                                            @foreach($alerts as $a)
                                                <li onclick="return readAlert({{ $a->id }})">
                                                    <a href="{{ url($a->action) }}">
                                                        <!-- <div class="task-icon badge badge-success text-center">&nbsp;</div> -->
                                                        <span class="badge badge-roundless badge-default pull-right">{{ $a->when }}</span>
                                                        <p class="task-details text-{{ $a->type or 'info' }}">{{ $a->body }}</p>
                                                    </a>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </li>
                                        @endif
                                        <li class="drop-all"><a href="#" class="text-center">All Alerts</a></li>
                                        <li onclick="return readAlert('all')" class="drop-all"><a href="#" class="text-center">Mark All As Read</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">Howdy, {{ \Auth::user()->fname }}!<i class="fa fa-angle-down"></i></span>
                                        <!-- Avatar -->
                                    </a>
                                    <ul class="dropdown-menu dropdown-list" role="menu">
                                        <li role="presentation"><a href="{{ url('admin/calendar') }}"><i class="fa fa-calendar"></i>Calendar</a></li>
                                        <li role="presentation"><a href="{{ url('admin/inbox') }}"><i class="fa fa-envelope"></i>Inbox</a></li>
                                        <li role="presentation"><a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('auth/logout') }}" class="log-out waves-effect waves-button waves-classic">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <ul class="menu accordion-menu">
                        <li><a href="{{ url('dashboard') }}" class="waves-effect waves-button"><span class="menu-icon icon-speedometer"></span><p>Dashboard</p></a></li>
                        <li><a href="{{ url('admin') }}" class="waves-effect waves-button"><span class="menu-icon icon-wrench"></span><p>Admin Panel</p></a></li>
                        <li><a href="{{ url('admin/users') }}" class="waves-effect waves-button"><span class="menu-icon fa fa-users"></span><p>Members</p></a></li>
                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-asterisk"></span><p>Manage Accounts</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('admin/withdrawal-requests') }}">Withdrawal Requests</a></li>
                                <li><a href="{{ url('admin/commission-payout-page') }}">Payout Commissions</a></li>
                            </ul>
                        </li>
                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-envelope"></span><p>Mailbox</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('admin/inbox') }}">Inbox</a></li>
                                <li><a href="{{ url('admin/compose') }}">Compose</a></li>
                            </ul>
                        </li>
                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon icon-settings"></span><p>Configuration</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('admin/settings') }}">Settings</a></li>
                                <li><a href="{{ url('admin/membership-levels') }}">Membership Levels</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div class="profile-cover">
                    <div class="row">
                        <div class="col-md-12 profile-info">
                            <div class=" profile-info-value">
                                <h3>{{ number_format($summary['all_members']) }}</h3>
                                <p>Members</p>
                            </div>
                            <div class=" profile-info-value">
                                <h3>{{ number_format($summary['all_managers']) }}</h3>
                                <p>Managers</p>
                            </div>
                            <div class=" profile-info-value">
                                <h3>{{ number_format($summary['all_pins']) }}</h3>
                                <p>Pins Sold</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="main-wrapper">
@yield('content')
                </div>
                <div class="page-footer">
                    <p class="no-s">{{ date('Y') }} &copy; Matrix Logic by Kajuzi.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->	

        <!-- Javascripts -->
        <script src="{{ asset('assets/admin/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/offcanvasmenueffects/js/classie.js') }}"></script>
        <!--<script src="{{ asset('assets/admin/plugins/offcanvasmenueffects/js/main.js') }}"></script>-->
        <script src="{{ asset('assets/admin/plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/3d-bold-navigation/js/main.js') }}"></script>
        <script src="{{ asset('assets/admin/js/modern.min.js') }}"></script>
        <!-- <script src="{{ asset('assets/admin/js/pages/profile.js') }}"></script> -->
        <script src="{{ asset('assets/admin/js/custom.js') }}"></script>

        @yield('js')
        @if(\Session::has('toastr'))
        <script type="text/javascript" src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                Command: toastr['{{ \Session::get('toastr')['type'] }}']("", "{{ \Session::get('toastr')['msg'] }}")

                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "7000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            });
        </script>
        @endif      
        <script>            
            function readAlert (id) {
                $.get('{{ url('alert/mark-as-read') }}'+'/'+ id, { id: id }, function (data) {
                    if(data.status == 'success'){
                        if(data.unread)
                            $('#countAlerts').html(data.unread);
                        else
                            $('#countAlerts').remove();
                    }
                });
                return true;
            }
        </script>          
    </body>
</html>