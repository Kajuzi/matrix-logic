@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Upload Proof Of Deposit</h4>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open(['url' => 'admin/proof-of-deposit', 'files' => true, 'class'=>'form-horizontal', 'method'=>'POST']) !!}
                                        <div class="form-group">
                                            <label for="amount" class="col-sm-2 control-label">Amount</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="amount" id="amount" placeholder="How much did you deposit?" type="text" required="required">
                                                {!! $errors->first('amount', "<p id=\"amount-error\" class=\"help-block text-danger\">:message</p>") !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="deposit_date" class="col-sm-2 control-label">Date of Deposit</label>
                                            <div class="col-sm-10">
                                                <input class="form-control date-picker" name="deposit_date" id="deposit_date" placeholder="yyyy-mm-dd hh:mm" type="text" required="required">
                                                <p id='deposit_date-info' class="help-block">Please make sure you enter the date in this form: {{ date('Y-m-d H:i') }}</p>
                                                {!! $errors->first('deposit_date', "<p id=\"deposit_date-error\" class=\"help-block text-danger\">:message</p>") !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="proof" class="col-sm-2 control-label">Proof</label>
                                            <div class="col-sm-10">
                                                {!! Form::file('proof', array('class' => 'form-control', 'id'=>'proof-file', 'onchange'=>"checkfile()", 'accepted'=>"application/pdf, image/jpeg, image/png", 'required' => "required")) !!}
                                                <p id="proof-info" class="help-block">Only PDF and JPEG allowed</p>
                                                {!! $errors->first('proof', '<p class="help-text text-danger">:message</p>') !!}
                                            </div>
                                        </div>
                                        {!! Form::hidden('transaction_id', $transaction_id) !!}
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Upload Proof!</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
@stop