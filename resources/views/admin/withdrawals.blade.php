@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">eWallet Withdrawal Requests</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Fullname</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                    <th>Bank</th>
                                                    <th>Acc. Num.</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!$requests)
                                            <tr>
                                                <td colspan="8">The website has no members</td>
                                            </tr>
                                            @else
                                                @foreach($requests as $key => $m)
                                                    <tr>
                                                        <th scope="row">{{ $key + 1 }}</th>
                                                        <td><a href="{{ url('members/profile/'.$m->id) }}">{{ $m->username }}</a></td>
                                                        <td><a href="{{ url('members/profile/'.$m->id) }}">{{ $m->fullname }}</a></td>                                                        
                                                        <td>{{ $m->amount }}</td>
                                                        <td>{{ $m->when }}</td>
                                                        <td>{{ $m->bank['title'] or 'N/A'}}</td>
                                                        <td>{{ $m->bank['acc_num'] or 'N/A'}}</td>
                                                        <td>
                                                            @if($m->proof)
                                                            Processed
                                                            @else
                                                            <a href="{{ url('admin/proof-of-deposit/'.$m->id) }}">Upload Proof</a> |
                                                            <a href="{{ url('admin/decline-deposit/'.$m->id) }}">Decline</a>
                                                            @endif
                                                        </td>
                                                        
                                                    </tr>                                                
                                                @endforeach
                                                <tr><td colspan="8" class="text-center"><a href="{{ url('admin/users') }}">See All</a></td>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
    <script>
        function announce () {
            var title = $('#announcement-title').val();
            var body = $('#announcement-body').val();
            var token = $('input[name=_token]').val();
            var announce = $('#announce');
            var parameters = { title : title, body : body, _token : token};

            $.post("announcements", parameters )
            .done(function(data) {
                announce.html('<h3 class="text-info">'+title+'</h3><p>'+body+'</p>');
            })
            .fail(function(data){
                announce.html('There has been an error. Please contact support');
            });
            return false;
        } 
    </script>
@stop