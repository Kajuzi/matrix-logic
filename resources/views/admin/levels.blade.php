@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Membership Levels</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table" style=".form-control { font-size: 10px; }">
                                            <thead>
                                                <tr>
                                                    <th>Level ID</th>
                                                    <th>Pay</th>
                                                    <th>Pin</th>
                                                    <th>Manager</th>
                                                    <th>Level</th>
                                                    <th>Upline</th>
                                                    <th>Downline Count</th>
                                                    <th>Total</th>
                                                    <th>Profit</th>
                                                    <th>Nett</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($levels as $key => $l)
                                                <tr>
                                                {!! Form::open() !!}
                                                    {!! Form::hidden('old_id', $l->id) !!}
                                                    <th scope="row">{!! Form::text('id', $l->id, ['class'=>'form-control']) !!}</th>
                                                    <td>{!! Form::text('pay', $l->pay, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('pin', $l->pin, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('manager', $l->manager, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('level', $l->level, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('upline', $l->upline, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('downline_count', $l->downline_count, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('total', $l->total, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('profit', $l->profit, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('nett', $l->nett, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::submit('Update', ['class'=>'btn btn-default btn-xs btn-primary']) !!} <a href="{{ url('admin/delete-level/'.$l->id) }}"><i class="fa fa-remove m-r-xs icon-state-danger"></i></a></td>
                                                {!! Form::close() !!}
                                                </tr>
                                                @endforeach
                                                <tr><td colspan="11" class="text-center">New Level</td>
                                                <tr>
                                                {!! Form::open() !!}
                                                    {!! Form::hidden('old_id', 'new') !!}
                                                    <th scope="row">{!! Form::text('id', null, ['class'=>'form-control']) !!}</th>
                                                    <td>{!! Form::text('pay', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('pin', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('manager', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('level', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('upline', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('downline_count', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('total', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('profit', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::text('nett', null, ['class'=>'form-control']) !!}</td>
                                                    <td>{!! Form::submit('Save', ['class'=>'btn btn-default btn-xs btn-primary']) !!}</td>
                                                {!! Form::close() !!}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
    <script>

    </script>
@stop