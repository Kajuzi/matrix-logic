@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-red">
                                <div class="panel-body">
                                    <h2 class="text-center">You're about to pay out {{ settings('currency_symbol') }} {{ number_format($sum) }} to {{ $all_members }} members<br />
                                    Be careful before you press the button</h2>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div id="commission_button" class="col-md-2 col-md-offset-5">
                                        <p class="text-center" id="reveal"><a href="javascript:void(0)">Reveal Button</a></p>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
<script type="text/javascript">
    $('#reveal').click(function () {
        $('#commission_button').html('<a href="{{ url('admin/commission-payout') }}" class="btn btn-danger btn-rounded btn-lg reg-btn"><span aria-hidden="true" class=" icon-share-alt"></span>  PAYOUT THE COMMISSIONS</a>')
    });
</script>
@stop