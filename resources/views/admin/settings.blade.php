@extends('admin.layout')
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Settings</h4>
                                </div>
                                <div class="panel-body">
                                    {!! \Form::open(["class"=>"form-horizontal"]) !!}
                                        @foreach($settings as $s)
                                            <div class="form-group">
                                                <label for="{{ $s['slug'] }}" class="col-sm-2 control-label">{{ $s['label'] }}</label>
                                                <div class="col-sm-10">
                                                    {!! $s['input'] !!}
                                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="col-sm-offset-2 col-sm-10">
                                            {!! Form::submit('Save Settings', ["class"=>"btn btn-primary"]) !!}
                                        </div>
                                        
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
    <script>
        function announce () {
            var title = $('#announcement-title').val();
            var body = $('#announcement-body').val();
            var token = $('input[name=_token]').val();
            var announce = $('#announce');
            var parameters = { title : title, body : body, _token : token};

            $.post("announcements", parameters )
            .done(function(data) {
                announce.html('<h3 class="text-info">'+title+'</h3><p>'+body+'</p>');
            })
            .fail(function(data){
                announce.html('There has been an error. Please contact support');
            });
            return false;
        } 
    </script>
@stop