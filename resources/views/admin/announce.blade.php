@extends('admin.layout')
@section('css')
    <link href="{{ url('assets/admin/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css">
@stop
@section('content')
                    <div class="row">
                        <div class="col-md-12 m-t-lg">
                            @if(Session::has('errors'))
                            <div class="panel panel-red">
                                <div class="panel-body">
                                    {{{ Session::get('errors')->first() }}}
                                    {!! $errors->first('title', ':message<br />') !!}
                                    {!! $errors->first('body', ':message<br />') !!}
                                    {!! $errors->first('publish_on', ':message<br />') !!}
                                    {{ $errors->first('expires_on') }}
                                </div>
                            </div>
                            @endif
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="post form-horizontal" id="announce">
                                        {!! Form::open(['url'=>url('announcements')]) !!}
                                            <div class="form-group">
                                                {!! Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Title', 'id'=>'announcement-title']) !!}
                                            </div>
                                            <div class="form-group">                                            
                                                <textarea name="body" id="announcement-body" class="form-control" placeholder="Post an announcement" rows="4=6"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Announcement Type</label>
                                                <div class="col-sm-10">
                                                    {!! Form::select('type', $type, 'text-info', ['class'=>'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Publish At</label>
                                                <div class="col-sm-10">
                                                    <input name="publish_at" class="form-control date-picker" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Expires At</label>
                                                <div class="col-sm-10">
                                                    <input name="expires_at" class="form-control date-picker" type="text">
                                                </div>
                                            </div>
                                            <div class="post-options">
                                                {!! Form::submit('Post',['class'=>"btn btn-default pull-right"]) !!}
                                            </div>
                                        {!! Form::close() !!}
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
@stop  

@section('js')
<script src="{{ asset('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {        
        $('.date-picker').datepicker({
            orientation: "top auto",
            autoclose: true,
            format: 'yyyy-mm-dd 00:00:00'
        });
    });    
</script>
@stop