<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Matrix Logic</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Matrix Logic - Key to financial freedom" />
        <meta name="keywords" content="landing" />
        <meta name="author" content="Kajuzi" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:500,400,300' rel='stylesheet' type='text/css'>
        <link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/animate/animate.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/tabstylesinspiration/css/tabs.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/tabstylesinspiration/css/tabstyles.css" rel="stylesheet" type="text/css">    
        <link href="assets/plugins/pricing-tables/css/style.css" rel="stylesheet" type="text/css">
        <link href="assets/css/landing.css" rel="stylesheet" type="text/css"/>
        
        <script src="assets/plugins/pricing-tables/js/modernizr.js"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .ol-loading {
                background-color: black;
                z-index: 2001;                
            }

            .ol-container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .ol-content {
                position: absolute;
                text-align: center;
                display: inline-block;
                width: 100%;
                height: 112px;
                top:45%;
            }

            .ol-title {
                font-size: 72px;
                margin-bottom: 40px;
            }            
        </style>
    </head>
    <body data-spy="scroll" data-target="#header">
        <nav id="header" class="navbar navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="#">Matrix Logic</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#features">Features</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="home" id="home">
            <div class="overlay"></div>
            <div class="overlay ol-loading">
                <div class="ol-body">
                    <div class="ol-container">
                        <div class="ol-content">
                            <div class="ol-title"><img src="assets/images/preloader.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="home-text col-md-8">
                        <h1 class="wow fadeInDown" data-wow-delay="1.5s" data-wow-duration="1.5s" data-wow-offset="10">Welcome To Matrix Logic</h1>
                        <p class="lead wow fadeInDown" data-wow-delay="2s" data-wow-duration="1.5s" data-wow-offset="10">Your key to financial freedom</p>
                        <a href="{{ url('login') }}" class="btn btn-default btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">&nbsp;&nbsp;Login&nbsp;&nbsp;</a>
                        <a href="#contact" class="btn btn-success btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">Contact Us</a>
                    </div>
                    <div class="scroller">
                        <a href="#features"><div class="mouse"><div class="wheel"></div></div></a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container" id="features">
            <div class="row features-list">
                <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
                    <div class="feature-icon">
                        <i class="fa fa-laptop"></i>
                    </div>
                    <h2>Convinient</h2>
                    <p>Do everything from the comfort of your home in you own time on your own device</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">
                    <div class="feature-icon">
                        <i class="fa fa-magic"></i>
                    </div>
                    <h2>Work Less</h2>
                    <p>It works like magic. Set it up and earn money while you do other thing</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
                    <div class="feature-icon">
                        <i class="fa fa-support"></i>
                    </div>
                    <h2>Free Support</h2>
                    <p>We are always there when you need help with anything on the website</p>
                    <p>&nbsp;</p>
                </div>
            </div>
        </div>
        <section id="section-1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                        <img src="assets/images/cute-dog.jpg" width="90%" class="iphone-img" alt="">
                    </div>
                    <div class="col-sm-8 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                        <h1>Let your investment work for you</h1>
                        <p>Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero.</p>
                        <ul class="list-unstyled features-list-2">
                            <li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Profitable</li>
                            <li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Friendly terms</li>
                            <li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Good returns</li>
                            <li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Easy to manage</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-3">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <p class="text-white">“This is the kind of investment opportunity I have always wanted. I can now have more time with my children while my investment grows on its own”</p>
                                            <span>- David Schrodinger</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <p class="text-white">“I'm now able to spend more time with my kids.”</p>
                                            <span>- Sarah Donector</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <p>“I love the interface. Makes everything a breeze”</p>
                                            <span>- Emily Kissinger</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </section>
        <div id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 wow rotateInUpLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
                        <a href="#contact" class="btn btn-success btn-lg btn-rounded contact-button"><i class="fa fa-envelope-o"></i></a>
                        <h2>Get in touch</h2>
                        <form class="m-t-md">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control input-lg contact-name" placeholder="Name">
                                    </div>
                                    <div class="col-sm-6">      
                                        <input type="email" class="form-control input-lg" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="4=6" placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-default btn-lg">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <p class="text-center no-s">2015 &copy; Matrix Logic     Site by <a href="http://kajuzi.com" target="_blank">Kajuzi</a>.</p>
            </div>
        </footer>
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/plugins/pace-master/pace.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/wow/wow.min.js"></script>
        <script src="assets/plugins/tabstylesinspiration/js/cbpfwtabs.js"></script>
        <script src="assets/plugins/pricing-tables/js/main.js"></script>
        <script src="assets/js/landing.js"></script>
        <script>
            $(document).ready(function () { 
                $('.ol-loading').delay(500).fadeTo('slow', 'toggle');
            });
        </script>
        
    </body>
</html>