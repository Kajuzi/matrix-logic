SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+02:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `body` varchar(512) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `read` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

INSERT INTO `alerts` (`id`, `recipient_id`, `type`, `body`, `action`, `read`, `created_at`, `updated_at`) VALUES
(23, 1, '', 'allison is requesting to purchase 2 pins', 'pins/requests', 0, '2015-12-31 04:43:44', '2016-01-04 04:59:38'),
(24, 1, '', 'You request to purchase 1 pins was declined', 'pins/history', 0, '2015-12-31 05:21:27', '2016-01-04 04:59:26'),
(25, 1, '', 'You request to purchase 2 pins was declined', 'pins/history', 0, '2015-12-31 05:21:49', '2016-01-04 04:59:07'),
(26, 1, '', 'You request to purchase 2 pins was declined', 'pins/history', 0, '2015-12-31 05:21:49', '2016-01-04 04:58:36'),
(27, 1, '', 'allison is requesting to purchase 1 pins', 'pins/requests', 0, '2015-12-31 06:53:20', '2016-01-04 04:58:56'),
(28, 13, 'danger', 'Your request to purchase 1 pins was declined', 'pins/history', 1, '2015-12-31 06:54:45', '2015-12-31 20:13:02'),
(29, 13, '', 'Your request to purchase 1 pins was declined', 'pins/history', 1, '2015-12-31 12:21:26', '2015-12-31 20:14:16'),
(30, 1, '', 'allison is requesting to purchase 1 pins', 'pins/requests', 0, '2015-12-31 12:45:59', '2016-01-04 04:58:46'),
(31, 13, '', 'Your request to purchase 1 pins was declined', 'pins/history', 1, '2015-12-31 12:47:21', '2016-01-12 16:54:15'),
(32, 1, 'info', 'allison is requesting to purchase 1 pins', 'pins/requests', 1, '2015-12-31 12:54:48', '2016-01-12 18:13:06'),
(33, 13, 'error', 'Your request to purchase 1 pins was declined', 'pins/history', 1, '2015-12-31 12:55:30', '2016-01-04 04:07:28');

CREATE TABLE IF NOT EXISTS `ew_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `ew_balances` (`id`, `amount`, `member_id`, `created_at`, `updated_at`) VALUES
(1, '1382', 1, '2015-11-10 00:06:13', '2015-11-10 00:06:13');

CREATE TABLE IF NOT EXISTS `ew_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `recipient_balance` decimal(15,0) NOT NULL,
  `sender_balance` decimal(15,0) NOT NULL,
  `description` varchar(255) NOT NULL,
  `proof` varchar(128) NOT NULL,
  `deposit_date` timestamp NULL DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

INSERT INTO `ew_transactions` (`id`, `amount`, `recipient_id`, `sender_id`, `recipient_balance`, `sender_balance`, `description`, `proof`, `deposit_date`, `approved`, `created_at`, `updated_at`) VALUES
(1, '450', 1, 4, '450', '-450', 'Purchasing 1 pin', '', '0000-00-00 00:00:00', 0, '2015-11-23 06:54:06', '2015-11-23 06:54:06'),
(2, '450', 1, 4, '900', '-900', 'Purchasing 1 pin', '', '0000-00-00 00:00:00', 0, '2015-11-23 06:55:57', '2015-11-23 06:55:57'),
(3, '1800', 1, 3, '2700', '-1800', 'Purchasing 4 pin', '', '0000-00-00 00:00:00', 1, '2015-11-23 07:01:03', '2015-11-23 07:01:03'),
(4, '450', 1, -1, '3150', '-450', 'eWallet top-up', 'dd76896bf7df8b9b9a4b6f5b0218f18f.png', '2015-11-26 08:25:00', 0, '2015-11-27 00:07:53', '2015-11-27 00:07:53'),
(5, '1350', -1, 1, '900', '1800', 'Purchasing 3 pins', '', '0000-00-00 00:00:00', 0, '2015-11-27 08:33:54', '2015-11-27 08:33:54'),
(6, '900', -1, 1, '1800', '900', 'Purchasing 2 pins', '', '0000-00-00 00:00:00', 1, '2015-12-16 01:43:21', '2015-12-16 01:43:21'),
(7, '2300', 1, -1, '3200', '-500', 'eWallet top-up', 'd95b1215806550366930a3078cbd3d10.pdf', '2015-12-07 22:00:00', 1, '2015-12-18 15:09:18', '2015-12-18 15:09:18'),
(8, '90', 0, 1, '90', '3110', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 11:05:10', '2015-12-26 11:05:10'),
(9, '90', 0, 1, '90', '3110', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 11:14:29', '2015-12-26 11:14:29'),
(10, '90', 0, 1, '90', '3110', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 11:16:54', '2015-12-26 11:16:54'),
(11, '90', 0, 1, '90', '3110', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 11:18:51', '2015-12-26 11:18:51'),
(12, '30', 1, 0, '3230', '-30', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 11:24:03', '2015-12-26 11:24:03'),
(13, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-26 14:15:43', '2015-12-26 14:15:43'),
(14, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 04:28:54', '2015-12-27 04:28:54'),
(15, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 04:36:07', '2015-12-27 04:36:07'),
(16, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 05:56:26', '2015-12-27 05:56:26'),
(17, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 05:57:01', '2015-12-27 05:57:01'),
(18, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 05:57:56', '2015-12-27 05:57:56'),
(19, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 05:59:15', '2015-12-27 05:59:15'),
(20, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 06:00:15', '2015-12-27 06:00:15'),
(21, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 06:01:33', '2015-12-27 06:01:33'),
(22, '60', 1, 0, '3260', '-60', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 08:48:33', '2015-12-27 08:48:33'),
(23, '7000', 13, -1, '7000', '-7500', 'eWallet top-up', 'f767c392a7d3875c88690efb7ea27e58.pdf', '2015-12-07 22:00:00', 1, '2015-12-27 11:06:44', '2015-12-27 11:06:44'),
(24, '60', 13, 1, '7060', '3140', 'pin sold', '', '0000-00-00 00:00:00', 0, '2015-12-27 11:08:32', '2015-12-27 11:08:32'),
(25, '90', 1, 13, '3290', '6910', 'pin sold', '', '0000-00-00 00:00:00', 1, '2015-12-27 12:16:21', '2015-12-27 12:20:21'),
(26, '60', 1, 13, '3260', '6940', 'pin transaction', '', '0000-00-00 00:00:00', 1, '2015-12-28 07:41:07', '2015-12-28 07:43:28'),
(27, '60', 1, 13, '3320', '6880', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-28 08:59:16', '2015-12-31 12:21:26'),
(28, '90', 1, 13, '3410', '6790', 'pin transaction', '', '0000-00-00 00:00:00', 1, '2015-12-28 09:15:05', '2015-12-28 09:15:32'),
(29, '60', 1, 13, '3470', '6730', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-28 09:17:45', '2015-12-31 05:21:26'),
(30, '90', 1, 13, '3560', '6640', 'pin transaction', '', '0000-00-00 00:00:00', 1, '2015-12-28 09:25:34', '2015-12-28 09:26:01'),
(31, '30', 1, 13, '3590', '6610', 'pin transaction', '', '0000-00-00 00:00:00', 1, '2015-12-28 11:03:00', '2015-12-28 11:04:32'),
(32, '500', 13, -1, '7110', '-8000', 'withdrawal', '', '0000-00-00 00:00:00', 0, '2015-12-28 18:49:58', '2015-12-28 18:49:58'),
(33, '500', 13, -1, '7110', '-8000', 'withdrawal', '', '0000-00-00 00:00:00', -1, '2015-12-28 19:05:28', '2015-12-31 02:23:35'),
(34, '700', 13, -1, '7310', '-8200', 'withdrawal', '', '0000-00-00 00:00:00', -1, '2015-12-28 19:07:23', '2015-12-31 01:21:33'),
(35, '700', 13, -1, '7310', '-8200', 'withdrawal', '', '0000-00-00 00:00:00', -1, '2015-12-28 19:07:42', '2015-12-31 01:14:51'),
(36, '520', -1, 13, '-6980', '6090', 'withdrawal', '7da7a85c4ff56c3b03ba47d39438389e.jpg', '0000-00-00 00:00:00', 1, '2015-12-28 19:26:31', '2015-12-31 00:48:29'),
(37, '60', 1, 13, '3650', '6030', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-31 04:41:52', '2015-12-31 05:21:49'),
(38, '60', 1, 13, '3650', '6030', 'pin transaction', '', '0000-00-00 00:00:00', 1, '2015-12-31 04:43:44', '2015-12-31 04:45:30'),
(39, '30', 1, 13, '3680', '6000', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-31 06:53:20', '2015-12-31 06:54:45'),
(40, '30', 1, 13, '3680', '6000', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-31 12:45:59', '2015-12-31 12:47:21'),
(41, '30', 1, 13, '3680', '6000', 'pin transaction', '', '0000-00-00 00:00:00', -1, '2015-12-31 12:54:48', '2015-12-31 12:55:30'),
(42, '500', -1, 13, '-6480', '5530', 'withdrawal', '', '0000-00-00 00:00:00', 0, '2016-01-07 04:28:44', '2016-01-07 04:28:44'),
(60, '1400', 13, 0, '7430', '-94437900', 'Commision', '', '0000-00-00 00:00:00', 1, '2016-01-15 12:13:22', '2016-01-15 12:13:22'),
(59, '94436500', 4, 0, '94436500', '-283309500', 'Commision', '', '0000-00-00 00:00:00', 1, '2016-01-15 12:13:21', '2016-01-15 12:13:21'),
(58, '94436500', 2, 0, '94436500', '-188873000', 'Commision', '', '0000-00-00 00:00:00', 1, '2016-01-15 12:13:21', '2016-01-15 12:13:21'),
(57, '94436500', 1, 0, '94440150', '-94436500', 'Commision', '', '0000-00-00 00:00:00', 1, '2016-01-15 12:13:21', '2016-01-15 12:13:21');

CREATE TABLE IF NOT EXISTS `levels` (
  `id` varchar(10) NOT NULL,
  `pay` int(11) NOT NULL,
  `pin` int(11) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `upline` int(11) DEFAULT NULL,
  `downline_count` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `profit` int(11) DEFAULT NULL,
  `nett` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `levels` (`id`, `pay`, `pin`, `manager`, `level`, `upline`, `downline_count`, `total`, `profit`, `nett`, `created_at`, `updated_at`) VALUES
('0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('1', 1200, 100, 200, 100, 800, 3, 2400, 1200, 0, '2016-01-26 02:05:56', '2016-01-26 02:11:07'),
('10', 24000, 2000, 4000, 2000, 16000, 59049, 94456500, 94454500, 1037430000, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('2', 1200, 100, 200, 100, 800, 9, 7200, 6000, 6000, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('3', 1200, 100, 200, 100, 800, 27, 21600, 20400, 25200, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('4', 2400, 200, 400, 200, 1600, 81, 129600, 127200, 152400, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('5', 2400, 200, 400, 200, 1600, 243, 388800, 386400, 538800, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('6', 2400, 200, 400, 200, 1600, 729, 1166400, 1164000, 1700400, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('7', 4800, 400, 800, 400, 3200, 2187, 6998400, 6993600, 8694000, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('8', 4800, 400, 800, 400, 3200, 6561, 20995200, 20990400, 29684400, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('9', 4800, 400, 800, 400, 3200, 19683, 62985600, 62980800, 92646000, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('a', 24000, 2000, 4000, 2000, 16000, 59049, 94456500, 94454500, 1037430000, '2016-01-26 02:05:56', '0000-00-00 00:00:00'),
('m', 24000, 2000, 4000, 2000, 16000, 59049, 94456500, 94454500, 1037430000, '2016-01-26 02:05:56', '2016-01-26 02:14:45');

CREATE TABLE IF NOT EXISTS `mailbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` varchar(1024) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `sender_trash` tinyint(1) NOT NULL,
  `recipient_trash` tinyint(4) NOT NULL DEFAULT '0',
  `starred` tinyint(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

INSERT INTO `mailbox` (`id`, `title`, `body`, `attachment`, `sender_id`, `recipient_id`, `read`, `sender_trash`, `recipient_trash`, `starred`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Welcome to the website', 'Congrats!', '', 3, 1, 1, 0, 0, 1, 'sent', '2015-12-15 20:24:45', '2015-11-13 04:35:21'),
(2, 'Welcome To The Website', '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $data[''folder''] = null;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $data[''subject''] = null;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $data[''unread''] = Mailbox::countUnread(\\Auth::id());<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $data[''summary''] = Admin::membershipSummary();<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; return view(''admin.compose'', $data);<br>&nbsp;&nbsp;&nbsp; }', '', 1, 13, 1, 0, 0, 0, 'sent', '2016-01-06 05:49:00', '2016-01-07 06:23:15'),
(3, 'Hello', 'Hello Allison<br>', '', 1, 13, 1, 0, 0, 0, 'sent', '2016-01-06 06:38:22', '2016-01-07 06:19:32');

CREATE TABLE IF NOT EXISTS `member_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `account_num` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_phone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `member_bank` (`id`, `member_id`, `title`, `account_num`, `account_name`, `account_phone`, `created_at`, `updated_at`) VALUES
(1, 1, 'POB', '0182324200', '', '0733211988', '2015-12-16 03:41:08', '2015-11-10 11:08:17');

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` varchar(1024) NOT NULL,
  `class` varchar(64) NOT NULL,
  `target` varchar(64) NOT NULL,
  `publish_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `messages` (`id`, `title`, `body`, `class`, `target`, `publish_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'title', 'body', 'text-info', 'members, managers', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-23 09:25:33', '2015-11-11 07:48:35'),
(2, 'New Payment Option', 'We are plaesed to announce that in addition to the many payment options we already had, we are adding the Payza for your convinience', 'text-info', '', '2016-11-07 08:00:00', '2015-11-29 22:00:00', '2016-01-15 12:08:26', '2015-11-11 06:04:29'),
(3, 'Allowed time for inactivity reduced', 'Please be advised that sarting form Nov 10, 2015 any account that goes for two callendar months without any activity will be automatically suspended', 'text-danger', '', '2015-11-07 08:00:00', '2016-11-07 08:00:00', '2016-01-15 12:11:09', '2015-11-11 06:04:29'),
(4, 'New Feature', 'To help you keep track of what''s going on with your account we have added the ability to export monthly reports to PDF', 'text-info', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-22 08:00:00', '2015-11-11 06:04:29'),
(9, 'Just One More Chance - The Midnighters', 'What a great classic soul jam', 'text-info', '', '2016-01-15 11:20:34', '2016-01-22 11:20:34', '2016-01-15 13:22:08', '2016-01-15 11:21:20');

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_06_223947_entrust_setup_tables', 2);

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'register-member', 'Register New Member', 'Register New Member', '2015-11-22 10:00:00', '2015-11-22 10:00:00'),
(2, 'edit-member', 'Edit Other Members Profile', 'Edit Other Members Profile', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2);

CREATE TABLE IF NOT EXISTS `pins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` varchar(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `available` tinyint(1) NOT NULL,
  `used` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

INSERT INTO `pins` (`id`, `pin`, `owner_id`, `available`, `used`, `created_at`, `updated_at`) VALUES
(1, '8f5123707d2da18d737cda11fcae162b', 13, 1, 0, '2015-11-09 14:21:44', '2015-12-28 09:02:13'),
(2, 'bc0796c9630debf50075b4dec3094367', 9, 1, 0, '2015-11-09 14:23:33', '2015-12-19 01:57:50'),
(3, 'acc05f7a7d4ae88ffaafb5101d1b4b54', 10, 1, 0, '2015-11-23 01:21:01', '2015-12-19 02:01:32'),
(4, '2f73976f284ad661e43fe406c79acb67', 11, 1, 0, '2015-11-23 01:21:01', '2015-12-19 02:42:10'),
(5, '0f18db78430bf57bba5b4429819a102e', 2, 1, 0, '2015-11-23 01:21:01', '2015-12-21 09:56:00'),
(6, '7b58917761be074229268ce1499739ba', 1, 1, 0, '2015-11-23 01:21:01', '2015-12-31 12:55:30'),
(7, '2fa222497f519485fbb36d7a1499137b', 3, 1, 0, '2015-11-23 01:21:01', '2015-12-21 10:51:25'),
(19, 'b7633aa812f08b7cd3e93bcd109a7b57', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(9, 'd4d73824b5cbc446e2dfe9e294c369a9', 13, 1, 0, '2015-11-27 08:29:28', '2015-12-28 09:15:32'),
(10, 'cb52b004580fd8beaf31e67f09c6e5e9', 1, 1, 0, '2015-11-27 08:29:28', '2015-12-31 12:45:59'),
(11, '3e21ed0225792df72daf950b2ad93388', 13, 1, 0, '2015-11-27 08:30:05', '2015-12-31 04:45:30'),
(12, 'a524122ff0c29c9d2d3f0e5fe18f0fa0', 13, 1, 0, '2015-11-27 08:30:05', '2015-12-23 01:26:50'),
(13, 'bd14419c3d50707ff5ca13bd484cc606', 13, 1, 0, '2015-11-27 08:30:05', '2015-12-28 09:18:11'),
(14, 'f461f6e7f3f83a6ff105be12cdf41d16', 13, 1, 0, '2015-11-27 08:33:54', '2015-12-31 04:45:30'),
(15, 'ea79661c6a2deb38ab73cf6facea13ef', 13, 1, 0, '2015-11-27 08:33:54', '2015-12-28 09:26:01'),
(16, '3ee9b2e5387e30d4a705f458ce91ebb1', 13, 1, 0, '2015-11-27 08:33:54', '2015-12-28 09:26:01'),
(17, 'b4dca211c70f0d1e7dbacc5a96d22fde', 13, 1, 0, '2015-12-16 01:43:21', '2015-12-28 09:26:01'),
(18, '07504a21a3f0b82c127912410e4494e4', 13, 1, 0, '2015-12-16 01:43:21', '2015-12-28 11:04:32'),
(20, '447080a2361de4ee735f792b0516aa1e', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(21, 'aec20ec1ca62b0dfb4b22d97a7562dd3', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(22, '5873600eecf0f30a5606c675a6cc87bd', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(23, 'dcd91e00a80f13b9135ff36d2b86bd33', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(24, 'fcc9ad289f742ada49d721d7b9586ff5', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(25, '9e1aeeef86a92600cd6e9146944e3e24', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(26, 'b9ec747f24707bccbf9fc49aadd27817', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(27, 'ca4e7665932c1a753fac2489fde94b8c', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(28, '4b315c133fc3254bcc3509fac6826fc1', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(29, '383338001f8e2b3e4713515c0f75849a', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(30, 'f8958252daae03066eb480552e63ef36', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(31, '1a2ed5d7115dfe02b04469aadf62b978', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05'),
(32, '59c681a44f86851905522bbd9935adc5', 2, 1, 0, '2016-01-04 15:36:05', '2016-01-04 15:36:05');

CREATE TABLE IF NOT EXISTS `pin_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `approved` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

INSERT INTO `pin_transfers` (`id`, `pin_id`, `sender_id`, `recipient_id`, `purpose`, `approved`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 'Downline donation', 1, '2016-02-03 13:25:05', '2016-02-03 11:25:05'),
(2, 2, 1, 3, 'Downline donation', 0, '2015-11-17 09:25:35', '2015-11-17 09:25:35'),
(3, 2, 1, 4, 'Registering toddy', 1, '2015-12-17 13:27:21', '2015-11-22 11:20:04'),
(4, 2, 1, 9, 'Registering sharon', 1, '2015-12-19 03:57:50', '2015-12-19 01:57:50'),
(5, 3, 1, -1, 'Registering rose', 1, '2015-12-21 12:10:06', '2015-12-19 02:01:32'),
(6, 4, 1, -1, 'Registering rose', 1, '2015-12-21 12:10:15', '2015-12-19 02:42:10'),
(7, 5, 1, 2, '', 1, '2015-12-21 11:56:00', '2015-12-21 09:56:00'),
(8, 6, 1, 2, '', 1, '2015-12-21 11:56:00', '2015-12-21 10:08:22'),
(9, 7, 1, 3, '', 1, '2015-12-21 12:51:25', '2015-12-21 10:51:25'),
(10, 8, 1, 9, 'Pin Donation', 1, '2015-12-21 13:54:00', '2015-12-21 11:54:00'),
(11, 9, 1, 2, 'asdgfdsgdfgf\r\n', 1, '2016-02-03 13:25:45', '2016-02-03 11:25:45'),
(12, 10, 1, 2, 'asdgfdsgdfgf\r\n', 0, '2015-12-21 17:00:08', '2015-12-21 17:00:08'),
(13, 11, 1, 2, 'asdgfdsgdfgf\r\n', 0, '2015-12-21 17:00:08', '2015-12-21 17:00:08'),
(14, 12, 1, 13, 'Registering allison', 1, '2015-12-23 03:26:50', '2015-12-23 01:26:50'),
(15, 13, 1, 0, 'pins sold', 1, '2016-02-03 13:27:34', '2016-02-03 11:27:34'),
(16, 14, 1, 0, 'pins sold', 0, '2015-12-26 11:04:50', '2015-12-26 11:04:50'),
(17, 15, 1, 0, 'pins sold', 0, '2015-12-26 11:04:50', '2015-12-26 11:04:50'),
(18, 16, 1, 0, 'pins sold', 0, '2015-12-26 11:05:10', '2015-12-26 11:05:10'),
(19, 17, 1, 0, 'pins sold', 0, '2015-12-26 11:05:10', '2015-12-26 11:05:10'),
(20, 18, 1, 0, 'pins sold', 0, '2015-12-26 11:05:10', '2015-12-26 11:05:10'),
(21, 1, 1, 13, 'pins sold', 0, '2015-12-27 11:48:46', '2015-12-27 11:48:46'),
(22, 16, 1, 13, 'pins sold', 0, '2015-12-27 11:48:46', '2015-12-27 11:48:46'),
(23, 17, 1, 13, 'pins sold', 0, '2015-12-27 11:50:00', '2015-12-27 11:50:00'),
(24, 1, 1, 13, 'pins sold', 0, '2015-12-27 12:16:12', '2015-12-27 12:16:12'),
(25, 15, 1, 13, 'pins sold', 0, '2015-12-27 12:16:12', '2015-12-27 12:16:12'),
(26, 1, 1, 13, 'pins transaction', 0, '2015-12-28 06:21:43', '2015-12-28 06:21:43'),
(27, 13, 1, 13, 'pins transaction', 0, '2015-12-28 06:21:43', '2015-12-28 06:21:43'),
(28, 14, 1, 13, 'pins transaction', 0, '2015-12-28 07:36:37', '2015-12-28 07:36:37'),
(29, 15, 1, 13, 'pins transaction', 0, '2015-12-28 07:36:37', '2015-12-28 07:36:37'),
(30, 16, 1, 13, 'pins transaction', 1, '2015-12-28 09:43:28', '2015-12-28 07:43:28'),
(31, 17, 1, 13, 'pins transaction', 1, '2015-12-28 09:43:54', '2015-12-28 07:43:54'),
(32, 1, 1, 13, 'pins transaction', 1, '2015-12-28 11:02:13', '2015-12-28 09:02:13'),
(33, 6, 1, 13, 'pins transaction', -1, '2015-12-31 14:21:26', '2015-12-31 12:21:26'),
(34, 9, 1, 13, 'pins transaction', 1, '2015-12-28 11:15:32', '2015-12-28 09:15:32'),
(35, 10, 1, 13, 'pins transaction', 0, '2015-12-28 09:15:05', '2015-12-28 09:15:05'),
(36, 11, 1, 13, 'pins transaction', 0, '2015-12-28 09:15:05', '2015-12-28 09:15:05'),
(37, 13, 1, 13, 'pins transaction', 1, '2015-12-28 11:18:11', '2015-12-28 09:18:11'),
(38, 14, 1, 13, 'pins transaction', -1, '2015-12-31 07:21:26', '2015-12-31 05:21:26'),
(39, 15, 1, 13, 'pins transaction', 1, '2015-12-28 11:26:01', '2015-12-28 09:26:01'),
(40, 16, 1, 13, 'pins transaction', 1, '2015-12-28 11:26:01', '2015-12-28 09:26:01'),
(41, 17, 1, 13, 'pins transaction', 1, '2015-12-28 11:26:01', '2015-12-28 09:26:01'),
(42, 18, 1, 13, 'pins transaction', 1, '2015-12-28 13:04:32', '2015-12-28 11:04:32'),
(43, 6, 1, 13, 'pins transaction', -1, '2015-12-31 07:21:49', '2015-12-31 05:21:49'),
(44, 10, 1, 13, 'pins transaction', -1, '2015-12-31 07:21:49', '2015-12-31 05:21:49'),
(45, 11, 1, 13, 'pins transaction', 1, '2015-12-31 06:45:30', '2015-12-31 04:45:30'),
(46, 14, 1, 13, 'pins transaction', 1, '2015-12-31 06:45:30', '2015-12-31 04:45:30'),
(47, 6, 1, 13, 'pins transaction', -1, '2015-12-31 08:54:45', '2015-12-31 06:54:45'),
(48, 10, 1, 13, 'pins transaction', -1, '2015-12-31 14:47:21', '2015-12-31 12:47:21'),
(49, 6, 1, 13, 'pins transaction', -1, '2015-12-31 14:55:30', '2015-12-31 12:55:30');

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Site Administrator', 'Has all privileges the site can offer users', '2015-11-06 19:24:49', '2015-11-06 19:24:49'),
(2, 'manager', 'Manager', 'Can manage and register other members', '2015-11-06 19:24:49', '2015-11-06 19:24:49'),
(3, 'member', 'Member', 'Ordinary registered member', '2015-11-06 19:24:49', '2015-11-06 19:24:49');

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 2),
(5, 3),
(6, 3),
(7, 3),
(9, 3),
(10, 3),
(11, 3),
(13, 3);

CREATE TABLE IF NOT EXISTS `settings` (
  `slug` varchar(50) NOT NULL,
  `title` varchar(128) NOT NULL,
  `type` varchar(12) NOT NULL,
  `value` varchar(512) NOT NULL,
  `options` varchar(1024) NOT NULL,
  `order` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`slug`, `title`, `type`, `value`, `options`, `order`, `description`, `created_at`, `updated_at`) VALUES
('auto_upgrade', 'Can Members Be Upgraded Automatically?', 'boolean', '1', '', 8, NULL, '2015-12-28 13:09:44', '2016-01-04 07:17:23'),
('currency', 'Default Currency', 'text', ' Malaysian Ringgit', '', 4, NULL, '2015-12-24 08:02:02', '2016-01-04 07:17:23'),
('currency_symbol', 'Default Currency Symbol', 'text', 'MYR', '', 5, NULL, '2015-12-24 08:02:02', '2016-01-04 07:17:23'),
('max_member_lev', 'Highest Member Level', 'select', '1', '1,2,3,4,5,6,7,8,9,10', 8, 'The highest level a member can attain before they become manager', '2016-01-04 08:45:37', '2016-01-04 07:17:24'),
('pin_price', 'Pin Price', 'float', '30', '', 1, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('require_email_validation', 'New Member Email Validation Required?', 'boolean', '1', '', 2, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('server_timezone', 'Default Server Timezone', 'select', 'America/New_York', 'Africa/Lagos,Africa/Cairo,Africa/Addis_Ababa,America/Adak,America/New_York,Europe/London,Asia/Beirut,Asia/Bangkok,Asia/Kuala_Lumpur,Asia/Ho_Chi_Minh', 3, NULL, '0000-00-00 00:00:00', '2016-01-04 07:17:23'),
('site_name', 'Name Of This Website', 'text', 'Matrix Logic', '', 0, NULL, '2015-12-24 07:21:31', '2016-01-04 07:17:23'),
('withdrawal_max', 'eWallet Withdrawal Maximum', 'text', '2000', '', 7, NULL, '2015-12-24 13:08:55', '2016-01-04 07:17:23'),
('withdrawal_min', 'eWallet Withdrawal Minimum', 'text', '500', '', 6, NULL, '2015-12-24 13:12:17', '2016-01-04 07:17:23');

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_id` int(11) NOT NULL,
  `level` int(1) DEFAULT '0',
  `upline` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `downline` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

INSERT INTO `users` (`id`, `username`, `fname`, `lname`, `gender`, `email`, `phone`, `country`, `manager_id`, `level`, `upline`, `downline`, `active`, `password`, `remember_token`, `last_activity`, `created_at`, `updated_at`) VALUES
(1, 'kaydee', 'Ken', 'Dan', 'male', 'ken@dan.com', '', NULL, 0, 1, '', '', 1, '$2y$10$ktX5MR1uSXOtc.Cy4DBmhOy2/5SkM89QRj/ZmOfUG37iFbN19WNCa', 'II1Uez4ncbWGcqZptHFREOYouqlvP2jTkySYz6YHyBc0ZdN8XbF7ImmguqHo', '2016-02-03 11:27:54', '2015-10-30 13:59:36', '2016-02-03 11:27:54'),
(2, 'sc', 'Stavo', 'Cazo', '', 'sc@matrix-login.com', '', NULL, 0, 0, '', '', 0, '$2y$10$76baDF872DrRPenSUTSgwOY6pW6aro4KTJtUuG2fc23oi6l4afgqa', 'eEofYZtsQvS8oTcJeYrHX4e8eNYX5RXAGEPrdpgHd78NNEEVndKEwnZtMkUC', '0000-00-00 00:00:00', '2015-11-02 06:33:04', '2015-11-02 06:35:15'),
(3, 'seth_bo', 'Seth', 'Bo', '', 'c@m.com', '', NULL, 0, 0, '', '', 0, '$2y$10$M.iJPqiQDe00ZD28a6zwgehocxHmqjPF50SBwVwfFn1LyEcRZsuTi', 'QTwzAzGiXJV5t3AqBoyOiN97KLIxuhzYHv1LLOZRYnFeL0VH9thS46WW2MgC', '0000-00-00 00:00:00', '2015-11-02 06:44:54', '2015-11-02 07:14:14'),
(4, 'ted', 'Ted', 'Mason', '', 'ot@ts.co', '23', NULL, 1, 0, '|1|', '', 1, '$2y$10$qtIUkaiXx6smRF1jIUROI.KSebM24Ko3LMJgpzaBY4xnV6X1eHJji', NULL, '0000-00-00 00:00:00', '2015-11-22 11:20:04', '2016-01-04 12:39:03'),
(5, 'roy', 'Roy', 'Ming', '', 'o@ts.co', '23', NULL, 1, 0, '|1|', '', 1, '$2y$10$irGjPapv3bKB1I6pRlc5ReM5IkRANuvcsqIIg4wy.EQ6nDdIBHd4W', NULL, '0000-00-00 00:00:00', '2015-11-22 11:16:16', '2015-11-22 11:16:16'),
(6, 'tod', 'Tod', 'Mase', '', 'o@t.co', '23', NULL, 4, 0, '|1|4|', '', 1, '$2y$10$qyAWF9xLJ4jXMitXmRZxEuNqwIG961wJrfDepejWp7J5FXBCZsyBW', NULL, '0000-00-00 00:00:00', '2015-11-22 11:14:59', '2015-11-22 11:14:59'),
(7, 'SamDong', 'Sam', 'Dong', '', 'sam@deug.com', '23', NULL, 6, 0, '|1|4|6|', '', 1, '$2y$10$PuQnKhuRq8dcC4LOCMFNrera8g5DA.CAxS/jBsGpEBvcHcj/Uk5gC', NULL, '0000-00-00 00:00:00', '2015-11-22 10:04:15', '2015-11-22 10:04:15'),
(-1, 'system', 'Matrix', 'Logic', 'female', 'system@matrix-logic.com', '', NULL, 0, 0, '', '', 0, 'no one can log in as system', 'no one can log in as system', '0000-00-00 00:00:00', '2015-10-30 13:59:36', '2015-12-16 01:41:08'),
(0, 'matrix-logic', 'Matrix', 'Logic', 'female', 'website@matrix-logic.com', '', NULL, 0, 0, '', '', 0, 'no one can log in as system', 'no one can log in as system', '0000-00-00 00:00:00', '2015-10-30 13:59:36', '2015-12-18 15:09:13'),
(9, 'sharon', 'Sharon', 'Mapuri', '', 'smaps@kodamail.com', '0773453423', NULL, 1, 0, '', '', 1, '$2y$10$0a0xW1nIkdivOzuyaOw.PO.gHXF7j97k2LZ6wkboGx9VBa0Uh5t9S', NULL, '0000-00-00 00:00:00', '2015-12-19 01:57:50', '2015-12-19 01:57:50'),
(11, 'rose', 'Rose', 'Chimhanga', '', 'rchim@webmail.com', '0773122122', NULL, 1, 0, '|1|', '', 1, '$2y$10$wSpjh3ld5fWSk1Y870eGsuU0rZErF.VTk.PeLWLzlvZIjzpBthz0O', NULL, '0000-00-00 00:00:00', '2015-12-19 02:42:10', '2015-12-19 02:42:10'),
(13, 'allison', 'Allison', 'w', '', 'allisonvbiz@gmail.com', '2354', NULL, 4, 1, '|1|4|', '', 1, '$2y$10$HcjZhouWr/PAixIQjlBTouS/F1moH1yMoEZaRKHtnlDfoPcs8kcyi', '1F2BrnpRiElDYvjHSlMc7ROr8ThOYehmMF5k5WKYfHdir7njGqd5fQX7JcmN', '2016-01-13 03:42:35', '2015-12-23 01:26:50', '2016-01-13 03:42:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
