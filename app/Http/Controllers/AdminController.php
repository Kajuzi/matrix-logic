<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Mailbox;
use App\Models\EwTransaction;

class AdminController extends Controller
{
    function __construct()
    {
        if (!\Entrust::hasRole('admin')) {
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'Forbidden Resource']);
        }
    }

    public static function getIndex()
    {
        $data['pending_trans'] = \App\Models\EwTransaction::allPendingApproval();
        $data['newbies'] = \App\Models\Member::newMembers(10);
        $data['summary'] = Admin::membershipSummary();
        return view('admin.dashboard', $data);
    }

    public static function getUsers()
    {
        $data['all_members'] = \App\Models\Member::allMembers();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.users', $data);
    }

    public static function getMembershipLevels()
    {
        $data['levels'] = \App\Models\Level::ese();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.levels', $data);
    }

    public static function getDeleteLevel($id)
    {
        if(\App\Models\Level::destroy($id))
            return redirect('admin/membership-levels')->with('toastr', ['type'=>'success', 'msg' => "Level $r->old_id has been updated"]);
        else
            return redirect('admin/membership-levels')->with('toastr', ['type'=>'error', 'msg' => 'Something Went Wrong']);
    }

    public static function postMembershipLevels(\App\Http\Requests\LevelsUpdateRequest $r)
    {
        if(\App\Models\Level::updateLevel($r))
            return redirect('admin/membership-levels')->with('toastr', ['type'=>'success', 'msg' => "Level $r->old_id has been updated"]);
        else
            return redirect('admin/membership-levels')->with('toastr', ['type'=>'error', 'msg' => 'Something Went Wrong']);
    }

    public static function getComposeMail()
    {
        $data['pending_trans'] = \App\Models\EwTransaction::allPendingApproval();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.dashboard', $data);
    }

    public static function getReplyMail($id)
    {
        $data['pending_trans'] = \App\Models\EwTransaction::allPendingApproval();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.dashboard', $data);
    }

    public static function getWithdrawalRequests()
    {
        $data['requests'] = EwTransaction::withdrawalRequests();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.withdrawals', $data);
    }

    public static function getProofOfDeposit($transaction_id)
    {
    	$data['transaction_id'] = $transaction_id;
        $data['summary'] = Admin::membershipSummary();
    	return view('admin.upload-proof', $data);
    }

    public static function postProofOfDeposit(\App\Http\Requests\AdminProofOfDeposit $r)
    {
		EwTransaction::adminProveDeposit($r->transaction_id);
        $data['summary'] = Admin::membershipSummary();
		return redirect('admin/withdrawal-requests');    	
    }

    public static function getDeclineDeposit($id)
    {
    	$trxn = EwTransaction::where('id', $id)
    							->where('description', 'withdrawal')
    							->first();
    	$trxn->approved = -1;
    	$trxn->update();
    	\App\Models\Alert::makeNew($trxn->sender_id, "Your withdrawal for $trxn->amount was declined");
    	return redirect('admin/withdrawal-requests');
    }
    
    public static function getAnnounce()
    {
        $data['summary'] = Admin::membershipSummary();
        $data['type'] = ['text-info'=>'General Announcement', 'text-danger'=>'Warning'];
        return view('admin.announce', $data);        
    }

    public function getSettings()
    {
        $data['settings'] = Admin::settings();
        $data['summary'] = Admin::membershipSummary();
        return view('admin.settings', $data);
    }

    public function postSettings(\App\Http\Requests\AdminSettingsRequest $r)
    {
        $setting = new \App\Models\Settings;
        unset($_POST['_token']);
        foreach ($_POST as $key => $val) {            
            $setting->where('slug', $key)->update(['value'=>$val]);
        }

       return redirect('admin/settings')->with('toastr', ['type'=>'success', 'msg' => 'Settings saved']);
    }

    public function getCommissionPayoutPage()
    {
        $data = Admin::commissionsSummary();
        $data['summary'] = Admin::membershipSummary();

        return view('admin.commissions-payout', $data);
    }

    public function getCommissionPayout()
    {
        Admin::payout();
        return redirect('admin')->with('toastr', ['type'=>'success', 'msg' => 'Commissions have been paid out']);
    }

    public function getInbox()
    {
        $data['messages'] = Mailbox::inbox(\Auth::id());
        $data['folder'] = 'inbox';
        $data['unread'] = Mailbox::countUnread(\Auth::id());
        $data['summary'] = Admin::membershipSummary();
        return view('admin.inbox', $data);
    }

    public function getSent()
    {
        $data['messages'] = Mailbox::sent(\Auth::id());
        $data['folder'] = 'sent';
        $data['unread'] = Mailbox::countUnread(\Auth::id());
        $data['summary'] = Admin::membershipSummary();
        return view('admin.inbox', $data);
    }

    public function getTrash()
    {
        $data['messages'] = Mailbox::trash(\Auth::id());
        $data['folder'] = 'trash';
        $data['unread'] = Mailbox::countUnread(\Auth::id());
        $data['summary'] = Admin::membershipSummary();
        return view('admin.inbox', $data);
    }

    public function getCompose()
    {
        $data['folder'] = null;
        $data['subject'] = null;
        $data['unread'] = Mailbox::countUnread(\Auth::id());
        $data['summary'] = Admin::membershipSummary();
        return view('admin.compose', $data);
    }

    public function postSend(\App\Http\Requests\SendmailRequest $r)
    {
        if(\App\Models\Mailbox::send($r->recipient, $r->subject, $r->msg))
            return redirect('admin/inbox')->with('toastr', ['type'=>'success', 'msg'=>'Message sent to '.$r->recipient]);
    }

    public function missingMethod($params = [])
    {
        return AdminController::getIndex();
    }

}
