<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Mailbox;

class MailboxController extends Controller
{
    public function getInbox()
    {
        $data = Auth::user();
        $data['messages'] = Mailbox::inbox($data->id);
        $data['folder'] = 'inbox';
        $data['unread'] = Mailbox::countUnread($data->id);
        return view('inbox', $data);
    }

    public function getSent()
    {
        $data = Auth::user();
        $data['messages'] = Mailbox::sent($data->id);
        $data['folder'] = 'sent';
        $data['unread'] = Mailbox::countUnread($data->id);
        return view('inbox', $data);
    }

    public function getTrash()
    {
        $data = Auth::user();
        $data['messages'] = Mailbox::trash($data->id);
        $data['folder'] = 'trash';
        $data['unread'] = Mailbox::countUnread($data->id);
        return view('inbox', $data);
    }

    public function getView($id)
    {
        $data = Auth::user();
        $data['message'] = Mailbox::singleMessage($id);
        $data['folder'] = null;
        $data['unread'] = Mailbox::countUnread($data->id);
        \DB::table('mailbox')->where('id', $id)
                        ->update(['read' => 1]);

        return view('view-message', $data);
    }


    public function getCompose()
    {
        $data = Auth::user();
        $data['subject'] = null;
        $data['folder'] = null;
        $data['unread'] = Mailbox::countUnread($data->id);
        return view('compose', $data);
    }

    public function getReply($id)
    {
        //
    }

    public function postSend(\App\Http\Requests\SendmailRequest $r)
    {
        if(\App\Models\Mailbox::send($r->recipient, $r->subject, $r->msg))
            return redirect('mailbox/inbox')->with('toastr', ['type'=>'success', 'msg'=>'Message sent to '.$r->recipient]);
        else
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg'=>'Some error occured']);
    }


    public function getDelete($id)
    {
        $msg = Mailbox::find($id);
        if (\Auth::id() != $msg->recipient_id && \Auth::id() != $msg->sender_id) {
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg'=>'You do not have privileges']);
        }else{
            if (Mailbox::sendToTrash($id)) 
                return \Redirect::back()->with('toastr', ['type'=>'success', 'msg'=>'Message deleted']);
            else
                return \Redirect::back()->with('toastr', ['type'=>'error', 'msg'=>'Some error occured']);

        }
    }
}
