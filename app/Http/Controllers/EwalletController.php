<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\EwTransaction;
use App\Http\Requests\Privileges;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use PDF;

class EwalletController extends Controller
{
    public static function getIndex()
    {
       return EwalletController::getShow(new Privileges, \Auth::id());
    }
    
    public static function getShow(Privileges $p, $id)
    {
        $data = EwTransaction::allData($id);
        return view('ewallet', $data);
    }
    
    public static function getAllPendingApproval($limit = 10, $page = 1)
    {
        $data = EwTransaction::allPendingApproval($limit, $page - 1);
        return view('ewallet', $data);
    }

    public static function postProof(Request $r)
    {
    	$field = 'proof';
    	if(EwTransaction::ProofAlreadyExists(\Auth::user()->id, $r->deposit_date))
    		return view('errors.general-error')->with('msg', "Proof already submited");
    	$proof = \App\Models\Handy::upload($field, 'uploads');
    	$trnxn = EwTransaction::makeNew(-1, \Auth::user()->id, $r->amount, 'eWallet top-up', $proof, $r->deposit_date);
    	if(!$trnxn)
    		return view('errors.general-error')->with('msg', "Something went wrong!!!");
    	return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => 'Success!']);
    }

    public static function postPurchasePins(\App\Http\Requests\PurchasePinsRequest $r)
    {
    	\App\Models\Pin::brandNew(\Auth::user()->id, $r->quantity)	;
    	EwTransaction::makeNew(\Auth::id(), -1, $r->quantity*\Config::get('settings.pin_price'), "Purchasing $r->quantity pins");
    	return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => "You successfully bought $r->quantity pins"]);
    }
    
    public static function getDecline($id) {
    	if (EwTransaction::decline($id)) {
    		return \Redirect::back();
    	};
    }
    
    public static function getApprove($id) {
        if (EwTransaction::approve($id)) {
            return \Redirect::back();
        };
    }
    
    public static function getDownloadPdf() {
        $data = EwTransaction::allTransactions(\Auth::id());

        // create some HTML content
        //return view('ewallet-pdf', $data);
        $html = view('ewallet-pdf', $data)->render();

        PDF::SetCreator('Matrix Logic');
        PDF::SetAuthor('Matrix Logic');
        PDF::SetTitle('Document Title');
        PDF::SetHeaderData("{asset('assets/images/signature.png')}", '250px', \App\Models\Member::fullname(\Auth::id()), 'eWallet History');

        PDF::AddPage();

        //dd($html);
        // output the HTML content
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::Output('ewallet-history-'.date('Y-m-d').'.pdf', 'D');

        return 'PDF file written';
    }
    
    public static function postWithdraw(\App\Http\Requests\WithdrawalRequest $r) {
        if (EwTransaction::requestWithdrawal($r->amount)) {
            return redirect('ewallet')->with('toastr', ['type' => 'success', 'msg' => 'Your withdrawal request has been successfully sent']);
        };
        return redirect('ewallet')->with('toastr', ['type' => 'error', 'msg' => 'Your withdrawal request was not processed']);
    }

    public static function getCommission()
    {
        $data = EwTransaction::commission(\Auth::id());
        return view('commission', $data);
    }

    public function missingMethod($path = '')
    {
    	return EwalletController::getShow(new Privileges, \Auth::id());
    }
}
