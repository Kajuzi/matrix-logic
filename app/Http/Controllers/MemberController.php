<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RegisterNewUser;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use App\User;
use Auth;
use App\Models\Member;
use App\Models\NewRegistration;
use App\Models\Pin;
use Input;
use PDF;

class MemberController extends Controller
{
    
    public function getIndex()
    {
        $all = Member::all();
        foreach ($all as $key => $member) {
            echo "$member->fname $member->lname  \t $member->username <br>";
        }
    }

    
    public function getRegister()
    {
        $data = Auth::user();
        return view('register', $data);
    }

    
    public function postRegister(RegisterNewUser $request)
    {
        if ($newbie = Member::newMember()) {
            return redirect('members/geneology/'.\Auth::id())->with('toastr', ['type'=>'success', 'msg' => 'User successfully registered'] );
        } else {
            return view('errors.general-error')->with('msg', 'Sorry. User not registered. Contact support');
        }
    }

    public function postNew(Request $request)
    {
        //
    }

    public function getProfile($id)
    {
        $data = User::find($id);
        if(!$data){
            return redirect('dashboard')->with('toastr', ['type'=>'error', 'msg'=>'User does not exist']);
        }
        $data['bank'] = \DB::table('member_bank')
                        ->where('member_id', $id)
                        ->first();
        $data['rank'] = Member::rank($id);
        return view('member-profile', $data);
    }

    public function getEdit($id)
    {

    }

    public function postUpdate(Request $request)
    {
        $info = User::find($request->id);
        $info->fname = $request->fname;
        $info->lname = $request->lname;
        $info->gender = $request->gender;
        $info->update();

        $bank['title'] = $request->title;
        $bank['account_num'] = $request->account_num;
        $bank['account_name'] = $request->account_name;
        $bank['account_phone'] = $request->account_phone;
        \DB::table('member_bank')->where('member_id', '=', $request->id)
                                ->update($bank);

        return redirect('dashboard')->with('toastr', ['type'=>'success', 'msg' => 'Profile Updated']);
    }

    public function getValidateEmail($token)
    {
        $newbie = NewRegistration::where('token', '=', $token)->first();
        if($newbie->token != $token){
            exit('Token not found');
        }
    }
    /**
    * Returns json*/
    public static function getGeneologyTree($id)
    {
        return Member::jsonTree($id);
    }

    public static function getGeneology($user_id)
    {
        $data = Member::simpleDownline($user_id);
        $data['table'] = Member::GeneologyTable($user_id)['downline'];
        return view('geneology', $data);
    }

    public static function getCommission()
    {
        $data = Member::simpleDownline(\Auth::id());
        return view('commission', $data);
    }

    public static function getAjaxFullname()
    {
        if(\Input::has('id')){
            $user = User::find(\Input::get('id'));
        }elseif(\Input::has('username')){
            $user = User::where('username', \Input::get('username'))->first();
        } else {
            exit('No Argument supplied');
        }

        if(!isset($user->fname)){
            exit("Member not found");
        }

        exit($user->fname.' '.$user->lname) ;
    }

    public static function getGeneologyTable($user_id)
    {
        $data = \App\Models\Member::GeneologyTable($user_id);
        return view('includes.geneology-table', $data);
    }

    public static function getUpgrade($user_id)
    {
        if(issuperior($user_id)){
            if(Member::upgrade($user_id))
                return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => 'Member Upgraded']);
            else
                return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'Member not upgraded']);
        }else{
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'You do not have the privileges for that']);
        }
    }

    public static function getActivate($user_id)
    {
        if(\Entrust::hasRole('admin')){
            $user = Member::find($user_id);
            $user->active = 1;
            $user->update();
            return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => 'Member has been activated']);
        }else{
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'You do not have the privileges for that']);
        }
    }

    public static function getDeactivate($user_id)
    {
        if(\Entrust::hasRole('admin')){
            $user = Member::find($user_id);
            $user->active = 0;
            $user->update();
            return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => 'Member suspended']);
        }else{
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'You do not have the privileges for that']);
        }
    }

    public static function getSuspend($user_id)
    {
        return redirect('members/deactivate/'.$user_id);
    }

    public function getDestroy($user_id)
    {
        if(\Entrust::hasRole('admin')){
            $user = Member::find($user_id);
            if (!$user) {
                return redirect('dashboard')->with('toastr', ['type'=>'error', 'msg' => 'That user does not exist']);
            }
            if ($user->active) {
                return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'The user has to be deactivated first']);
            } else {
                $user->destroy($user_id);
                return \Redirect::back()->with('toastr', ['type'=>'success', 'msg' => 'The member has been deleted']);
            }
        }else{
            return \Redirect::back()->with('toastr', ['type'=>'error', 'msg' => 'You do not have the privileges to delete users']);
        }
    }
}
