<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Alert;

class AlertController extends Controller
{
    /*
    * Returns all alerts for the user
    */
    public static function getIndex()
    {
        $data = \Auth::user();
        $data['alerts'] = Alert::allOfThem(30, true);
        AlertController::getMarkAsRead('all');

        return view('alerts', $data);
    }

    /*
    * Mark the alert with the given id as read
    */
    public static function getMarkAsRead($id)
    {
        Alert::markAsRead($id);
        $a['status'] = 'success';
        $a['unread'] = Alert::countUnread();
        return $a;
    }
}
