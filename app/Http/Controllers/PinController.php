<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BuyPinsRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Pin;
use App\Models\PinTransfer;
//use App\User;

class PinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $data = Auth::user();
        $data['transfers'] = PinTransfer::history($data['id']);
        return view('pin', $data);
    }

    /**
     * Show the form for transfering pins.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTransfer()
    {
        $data = Auth::user();
        $data['pins'] = Pin::allPins($data['id']);
        return view('pin-transfer', $data);
    }

    /**
     * Transfer pins.
     *
     */
    public function getRequestTransfer()
    {
        $data = Auth::user();
        $pins = Pin::countAvailable($data['id']);

        $recipient = \Input::get('recipient');
        $amount = \Input::get('amount');
        $purpose = \Input::get('purpose');
        $password = \Input::get('password');

        if (\App\User::names($recipient) == 'User not found') {
            exit('That recipient does not exist'); // TODO: More graceful handling of this
        }

        if ($amount > $pins) {
            exit('Not enough pins for this transaction!');
        }

        if ($data['username'] == $recipient) {
            exit('Sending to yourself does not make sense');
        }

        $pin = Pin::requestTransfer($data['username'], $recipient, $purpose);

        return view('pin', $data)->with('toastr', ['type'=>'success', 'msg' => 'The pin has been transferred']);
    }

    /**
     * History of pins
     */
    public function getHistory($id = null)
    {
    	if ($id === null)
    		$id = \Auth::id();
        $data = Auth::user();
        $data['history'] = PinTransfer::history($id);
        $data['pending'] = PinTransfer::pending($id);
        return view('pin-history', $data);
    }

    /**
     * Transfer pins.
     */
    public function postTransfer()
    {
        $data = Auth::user();
        $pins = Pin::countAvailable($data['id']);

        $recipient = \Input::get('recipient');
        $amount = \Input::get('amount');
        $password = \Input::get('password');
        $purpose = \Input::get('purpose');

        if (\App\User::names($recipient) == 'User not found') {
            return view('errors.general-error')->with('msg', 'That recipient does not exist'); // TODO: More graceful handling of this
        }

        if ($amount > $pins) {
            return view('errors.general-error')->with('msg', 'Not enough pins for this transaction!');
        }

        if ($data['username'] == $recipient) {
            return view('errors.general-error')->with('msg', 'Sending to yourself does not make sense');
        }

        $pin = Pin::requestTransfer($data['username'], $recipient, $purpose, $amount);

        return redirect('pins/history/'.\Auth::id())->with('toastr', ['type' => 'success', 'msg' => "$amount pins have been transferred to $recipient"]);
    }

    public function getRequestPurchase($amount)
    {
        if (\Entrust::hasRole('admin')) {
            Pin::transfer('matrix-logic', \Auth::user()->username, '', $amount);
        } else {
            dd('not admin');
        }
        if(PinTransfer::requestPurchase($amount)){
            return redirect('pins/history/'.\Auth::id())->with('toastr', ['type' => 'success', 'msg' => "Pin purchase request successful"]);
        }else{
            return redirect('dashboard')->with('toastr', ['type' => 'error', 'msg' => "There was an error. Request not processed"]);
        }
    }

    public function getPinPurchaseRequests($id)
    {
        if(!\App\Models\Member::GeneologyCount($id))
            return redirect('dashboard')->with('toastr', ['type' => 'error', 'msg' => "You do not have any downline"]);
        $data  = \App\Models\Member::find($id);
        $data['requests'] = PinTransfer::purchaseRequests($id);
        return view('pin-requests', $data);
    }

    /**
     * Approve a requested purchase
     */
    public function getApprovePurchase($id, $quantity)
    {
        if (PinTransfer::approvePurchase($id, $quantity)) {
            echo "success";
        } else {
            echo "system error";
        }
        
    }

    /**
     * Decline a requested purchase
     */
    public function getDeclinePurchase($id, $quantity)
    {
        if (PinTransfer::declinePurchase($id, $quantity)) {
            echo "success";
        } else {
            echo "system error";
        }
        
    }

    /**
     * Display form for pin purchase
     */
    public function getBuy()
    {
        $data = Auth::user();
        $data['upline_pins'] = Pin::countAvailable(\Auth::user()->manager_id);
        $data['balance'] = \App\Models\EwTransaction::balance(\Auth::id());
        $affordable = floor($data['balance']/settings('pin_price'));
        $data['max_pins'] = ($affordable < $data['upline_pins']) ? $affordable : $data['upline_pins'];
        if(!$data['upline_pins'])
            return redirect('dashboard')->with('toastr', ['type' => 'error', 'msg' => 'Your upline does not have enough pins']);
        if($affordable < 1)
            return redirect('dashboard')->with('toastr', ['type' => 'error', 'msg' => 'You don\'t have enough funds']);
        return view('buy-pin', $data);
    }

    /**
     * Does something
     */
    public function postBuy(BuyPinsRequest $r)
    {
        
    	return PinController::getRequestPurchase($r->quantity);
    }

    /**
     * Award Pins to a member for free
     */
    public function postAward(\App\Http\Requests\AwardPinsRequest $r)
    {
        if(!$r->quantity)
            return \Redirect::back()->with('toastr', ['type' => 'error', 'msg' => 'No Pins Awarded']);
        if(Pin::brandNew($r->user_id, $r->quantity)){
            alert($r->user_id, "Admins have awarded you $r->quantity pins", $action='pins/history', $type='info');
            return \Redirect::back()->with('toastr', ['type' => 'success', 'msg' => 'Pins Successfully Awarded']);
        }else{
            return \Redirect::back()->with('toastr', ['type' => 'error', 'msg' => 'Internal Error Occured']);
        }

    }

    /**
     * Display form for pin purchase
     */
    public function missingMethod($params = [])
    {
        return PinController::getIndex();
    }


}
