<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('mail', function () {
    return \App\Models\Mailbox::inbox();
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('welcome');
});

Route::get('pin/new/{owner_id}/{amount}', function ($owner_id, $amount) {
    return \App\Models\Pin::brandNew($owner_id, $amount);
});

Route::group(['middleware'=>'auth'], function () {
    Route::controller('members', 'MemberController');
    Route::controller('pins', 'PinController');
    Route::controller('ewallet', 'EwalletController');
    Route::controller('mailbox', 'MailboxController');
    Route::controller('alert', 'AlertController');
    Route::resource('announcements', 'AnnouncementController');
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('users', ['as'=>'users', function() { return "users";  }]);
});

Route::group(['middleware' => 'admin'], function (){//Middleware should be admin
    Route::controller('admin', 'AdminController');
});

Route::get('downline', ['middleware' => 'auth', function () {
	return \App\Models\Member::GeneologyTable(1);
}]);

Route::get('compose', ['middleware' => 'auth', function () {
    $data = Auth::user();
    return view('compose', $data);
}]);

Route::get('names/{id}', ['middleware' => 'auth', function ($id) {
	return \App\User::names($id);
}]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('roles', 'RoleController@index');

Route::get('drop', function(){
    $tables = ['alerts','ew_balances', 'ew_transactions', 'mailbox', 'member_bank', 'messages', 'migrations', 'password_resets', 'permissions', 'permission_role', 'pins', 'pin_transfers', 'roles', 'role_user', 'users', 'settings'];
    foreach ($tables as $t) {
        \Schema::drop($t);
    }
});