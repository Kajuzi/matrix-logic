<?php

function ago($timestamp)
{
	return \App\Models\Handy::ago($timestamp);
}

function toTime($timestamp)
{
	return \App\Models\Handy::toTime($timestamp);
}

function settings($slug)
{
	return \App\Models\Settings::val($slug);
}

function lastActivity($id)
{
	$user = Auth::user();
	$now = new DateTime();
	$user->last_activity = $now;
	$user->save();
}

function balance($user_id)
{
	return \App\Models\EwTransaction::balance($user_id);
}

function username($user_id)
{
	return \App\Models\Member::username($user_id);
}

function fullname($user_id)
{
	return \App\Models\Member::fullname($user_id);
}

function upload($field, $destination = 'uploads')
{
	return \App\Models\Handy::upload($field, $destination);
}

function alert($user_id, $body, $action='', $type='info')
{
	return \App\Models\Alert::makeNew($user_id, $body, $action, $type);
}

function alerts()
{
	return \App\Models\Alert::allOfThem();
}

function issuperior($id)
{
	return \App\Models\Member::issuperior($id);
}

function objectToArray($object)
{
	$arr = [];
	foreach ($object as $key => $value) {
		if(is_object($value))
			$arr[$key] = objectToArray($value);
		else
			$arr[$key] = $value;
	}
	return $arr;	
}