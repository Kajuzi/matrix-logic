<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PurchasePinsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $pw = (\Input::get('password'));
        if(!\Auth::attempt(['id' => \Auth::user()->id, 'password' => $pw]))
        {
            return view('errors.general-error')->with('msg', 'Wrong password');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
