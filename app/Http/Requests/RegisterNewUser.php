<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Pin;
use Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class RegisterNewUser extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!Pin::countAvailable(Auth::user()->id)){
            return view('errors.general-error', ['msg' => 'You do not have a pin avalable for this transaction']);
        }
        if(\Entrust::can('register-member')) {
            return true;
        }else{
            return false;
        }        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required|min:2|max:255',
            'lname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|min:2|unique:users',
            'password' => 'required|confirmed|min:6',
        ];
    }
}
