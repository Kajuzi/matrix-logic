<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminProofOfDeposit extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proof' => 'required|mimes:jpeg,jpg,png,pdf|max:500',
            'amount' => 'numeric',
            'deposit_date' => 'date_format:Y-m-d H:i'
        ];
    }
}
