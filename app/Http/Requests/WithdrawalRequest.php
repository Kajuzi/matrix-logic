<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WithdrawalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        if (\Auth::attempt(['email' => $user->email, 'password' => \Input::get('password')]))
            return true;
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|between:'.settings('withdrawal_min').','.settings('withdrawal_max')
        ];
    }
}
