<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostAnnouncementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Entrust::hasRole('admin')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'body' => 'required',
            'publish_at' => 'required|date_format:Y-m-d H:i:s|after:'.date('Y-m-d', time()-24*60*60),
            'expires_at' => 'required|date_format:Y-m-d H:i:s|after:'.\Input::get('publish_at')
        ];
    }
}
