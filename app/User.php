<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, 
        EntrustUserTrait {
            EntrustUserTrait::can insteadof Authorizable;
        }//;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'fname', 'lname', 'gender', 'email', 'phone', 'country', 'manager_id', 'upline', 'downline', 'active', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function names($id)
    {
        
        if (is_numeric($id)) {
            $user = User::find($id);
        } else {
            $user = User::where('username', '=', $id)->first();
        }
        
        if ($user) {
            $names['full'] = $user->fname." ".$user->lname;
            $names['username'] = $user->username;            
        } else {
            $names['full'] = "User not found";
            $names['username'] = "N/A";  
        }  

        return $names;
    }
}
