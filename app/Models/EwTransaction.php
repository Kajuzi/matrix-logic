<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Member;
use App\Models\Pin;

class EwTransaction extends Model
{
    public static function makeNew ($sender_id, $recipient_id, $amount, $description, $proof = '', $deposit_date='')
    {
    	$transaction = new EwTransaction;
    	$transaction->sender_id = $sender_id;
    	$transaction->recipient_id = $recipient_id;
    	$transaction->amount = $amount;
        $transaction->description  = $description;
        $transaction->proof  = $proof;
    	$transaction->deposit_date  = $deposit_date;
    	$transaction->sender_balance  = EwTransaction::balance($sender_id) - $amount;
    	$transaction->recipient_balance  = EwTransaction::balance($recipient_id) + $amount;
        $transaction->save();
        return $transaction;
    }

    public static function balance($user_id)
    {
    	$lastTransaction = EwTransaction::whereRaw("(recipient_id = $user_id or sender_id = $user_id) and approved = 1")
    										->orderBy('created_at', 'DESC')
    										->first();
    	if(!$lastTransaction)
    		return 0;
    	
    	$balance = ($lastTransaction->recipient_id == $user_id) ? $lastTransaction->recipient_balance : $lastTransaction->sender_balance;
    	return $balance;
    }

    public static function allTransactions($user_id)
    {
    	$monthago = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME'] - 30*24*60*60);
    	$data['approved'] = EwTransaction::whereRaw("(sender_id = $user_id or recipient_id = $user_id) and approved = '1' and created_at > '$monthago'")
    							->orderBy('created_at', 'DESC')
    							->get();
    	$data['pending'] = EwTransaction::whereRaw("(sender_id = $user_id or recipient_id = $user_id) and approved = '0' and created_at > '$monthago'")
    							->orderBy('created_at', 'DESC')
    							->get();
    	$data['declined'] = EwTransaction::whereRaw("(sender_id = $user_id or recipient_id = $user_id) and approved = '-1' and created_at > '$monthago'")
    							->orderBy('created_at', 'DESC')
    							->get();
    	return $data;
    }

    public static function allData($user_id)
    {
    	$data = User::find($user_id)->toArray();
    	$data['balance'] = EwTransaction::balance($user_id);
    	$data['transactions'] = EwTransaction::allTransactions($user_id);
    	foreach ($data['transactions'] as $k => $type) {
	    	foreach ($type as $key => $t) {
	    		$data['transactions'][$k][$key]->ago = Handy::ago($t->created_at);
	    		if ($t->sender_id == $user_id) {
	    			$data['transactions'][$k][$key]->amount = '<span class="text-danger">- '.number_format($t->amount).'<span>';
		    		$data['transactions'][$k][$key]->balance = number_format($t->sender_balance);
	    		} else {
	    			$data['transactions'][$k][$key]->amount = number_format($t->amount);
		    		$data['transactions'][$k][$key]->balance = number_format($t->recipient_balance);
	    		}
	    	}
    	}
    	$affordable = balance($user_id)/settings('pin_price');
    	$data['withdrawal_min'] = settings('withdrawal_min');
    	$data['withdrawal_max'] = (balance($user_id) < settings('withdrawal_max'))?balance($user_id):settings('withdrawal_max');
    	$data['max_pins'] = ($affordable < Pin::countAvailable($data['manager_id']))?$affordable:Pin::countAvailable($data['manager_id']);

    	return $data;
    }

    public static function ProofAlreadyExists()
    {
        $existing = EwTransaction::whereRaw("recipient_id = ".\Auth::user()->id." and deposit_date = '".\Input::get('deposit_date')."' and amount = ".\Input::get('amount'))->first();
        if($existing)
            return true;
        else
            return false;
    }

    public static function approve($id)
    {
        $trxn = EwTransaction::find($id);
        $approve = \DB::table('ew_transactions')->where('id', $id)->update(['approved' => 1]);
        if($trxn){
            $trxn->approved = 1;
            $trxn->save();
            if (settings('auto_upgrade')) {
                Member::upgrade('recipient_id');
                Member::upgrade('sender_id');
            }
            return true;
        }else{
            return false;
        }
    }

    public static function decline($id)
    {
        $decline = \DB::table('ew_transactions')->where('id', $id)->update(array('approved' => -1));
        if($decline)
            return true;
        else
            return false;
    }

    public static function allPendingApproval($limit = 10, $page = 0) {
    	$monthago = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME'] - 30*24*60*60);
    	$data = EwTransaction::where("approved", '=', 0)
    							->where("created_at", '>', $monthago)
    							->orderBy('created_at', 'DESC')
    							->skip(0*$page)
    							->take($limit)
    							->get();
    	foreach($data as $key => $row){
    		$data[$key]->sender = \App\User::find($row->sender_id);
    		$data[$key]->recipient = \App\User::find($row->recipient_id);
    		$data[$key]->ago = \App\Models\Handy::ago($row->created_at);
    	}
    	return $data;
    }

    public static function requestWithdrawal($amount)
    {
        return EwTransaction::makeNew(\Auth::id(), -1, $amount, 'withdrawal');
    }

    public static function withdrawalRequests()
    {
    	$monthago = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME'] - 30*24*60*60);
    	$data = EwTransaction::where("approved", 0)
    							->where('description', 'withdrawal')
    							->orderBy('created_at', 'ASC')
    							->get();
    	foreach($data as $key => $row){
    		$data[$key]->bank = \DB::table('member_bank')->where('member_id', $row->sender_id)->first();
    		$data[$key]->fullname = fullname($row->sender_id);
    		$data[$key]->username = username($row->sender_id);
    		$data[$key]->when = ago($row->created_at);
    	}
    	return $data;
    }

    public static function adminProveDeposit($id)
    {
    	$trxn = EwTransaction::where('id', $id)
						    	->where('description', 'withdrawal')
						    	->first();
    	if(!trxn)
    		return false;
    	$trxn->proof = upload('proof');
    	if(!$trxn->proof)
    		return false;
    	$trxn->approved = 1;
    	$trxn->update(); 
    	\App\Models\Alert::makeNew($id, "Your withdrawal for $trxn->amount was processed", 'ewallet/history/'.$trxn->sender_id);
    	return true;
    }

    public static function commission($user_id)
    {
        $data = Member::simpleDownline(\Auth::id());
        $levels = \App\Models\Level::ese();
        $data['downline_contributions'] = 0;
        foreach ($data['downline'] as $dl){
        	$data['downline_contributions'] =+ $levels[Member::rankId($user_id)]->manager;
        }
        $data['pay'] = $levels[Member::rankId($user_id)]->pay;
        $data['total'] = $levels[Member::rankId($user_id)]->total;
        $data['sum'] = $data['downline_contributions'] + $data['total'] - $data['pay'];
        
        return $data;
    }
}
