<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    public static function makeNew($recipient_id, $body, $action='', $type='info')
    {
    	$alert = new Alert;

    	$alert->recipient_id = $recipient_id;
    	$alert->body = $body;
        $alert->action = $action;
    	$alert->type = $type;

    	$alert->save();
    }
    
    public static function allOfThem($count=5, $read = 0) {
    	$data = [];
    	if (!$read) {
    		$all = Alert::where('recipient_id', \Auth::id())
    						->where('read', 0)
    						->orderBy('created_at', 'DESC')
    						->take($count)
    						->get();
    	}else{
    		$all = Alert::where('recipient_id', \Auth::id())
				    		->orderBy('created_at', 'DESC')
				    		->take($count)
				    		->get();
    	}
    	if(!$all)
    		return $data;
    	foreach ($all as $key => $alert){
    		$alert->when = ago($alert->created_at);
    		$data[] = $alert;
    	}
    	  	
    	return $data;			
    }

    public static function markAsRead($id)
    {
        if ($id == 'all'){
            $all = Alert::all();
            foreach ($all as $key => $alert) {
                $alert->read = 1;
                $alert->update();
            }
            return true;
        }else{
            $alert = Alert::find($id);
            $alert->read = 1;
            $alert->update();
            return true;
        }
    }

    public static function countUnread()
    {
        return Alert::where('recipient_id', \Auth::id())->where('read', 0)->count();
    }
}
