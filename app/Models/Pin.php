<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Pin extends Model
{
	public static function brandNew($owner_id, $amount = 1){
		for ($i=0; $i < $amount; $i++) { 
			$pin = new Pin;

			$pin->owner_id = $owner_id;
			$pin->pin = md5($owner_id.microtime().rand(1,1000));
			$pin->available = 1;
			$pin->save();
		}
		return "$amount pins created!";
	}

	public static function countAvailable($id){
		$pins = Pin::whereRaw("owner_id = {$id} and available = 1 and used != 1")->count();
		if($id == 0){
			$pin_threshold = \App\Models\Settings::val('system_pin_threshold');
			$short = $pin_threshold - $pins;
			if($short){
				Pin::brandNew(0, $short);
				$pins = $pin_threshold;
			}
		}
		return $pins;
	}

	public static function countPins($id){
		return Pin::where("owner_id", '=', $id)->count();
	}

	public static function allPins($id){
		$pins['all'] = Pin::countPins($id);
		$pins['available'] = Pin::countAvailable($id);

		return $pins;
	}

	public static function requestTransfer($sender_username, $recipient_username, $purpose = "", $amount = 1){
		$sender = User::where('username', $sender_username)->first();
		$recipient_id = ($recipient_username == 'matrix-logic') ? '0' : User::where('username', $recipient_username)->first()->id;
		$available_pins = Pin::countAvailable($sender->id);

		if(!$available_pins || $available_pins < $amount){
			return false;
		}

		for ($i=0; $i < $amount; $i++) { 
			$pin = Pin::whereRaw("owner_id = $sender->id and available = 1 and used != 1")->first();
			$pin->available = 0;
			$pin->update();

			$transfer = new PinTransfer;
			$transfer->sender_id = $sender->id;
			$transfer->pin_id = $pin->id;
			$transfer->recipient_id = $recipient_id;
			$transfer->purpose = $purpose;
			$transfer->save();
		}
		return true;
	}

	/**
	* Automatically authorise the transfer
	*/
	public static function transfer($sender_username, $recipient_username, $purpose = '', $amount = 1){
		Pin::requestTransfer($sender_username, $recipient_username, $purpose, $amount);
		$sender = User::where('username', $sender_username)->first();
		$recipient_id = ($recipient_username == 'matrix-logic') ? '0' : User::where('username', '=', $recipient_username)->first()->id;
		$transfer = PinTransfer::whereRaw("sender_id = $sender->id and recipient_id = $recipient_id and purpose = '$purpose' and approved = 0")->orderBy('created_at','desc')->take($amount)->get();
		foreach ($transfer as $transf):
			$transf->approved = 1;
			$transf->update();
			$pin = Pin::find($transf->pin_id);
			$pin->owner_id = $recipient_id;
			$pin->available = 1;
			$pin->used = ($recipient_username == 'matrix-logic') ? 1 : 0 ;
			$pin->update();
		endforeach;

		return true;
	}

	public static function approveTransfer($transfer_id){
		$transfer = PinTransfer::find($transfer_id);
		$transfer->approved = 1;
		$transfer->update();

		$pin = Pin::find($transfer->pin_id);
		$pin->owner_id = $transfer->recipient_id;
		$pin->available = 1;
		$pin->update();

		return true;
	}

	public static function useOne($owner_id, $newbie){
		Pin::transfer(User::find($owner_id)->username, 'matrix-logic', "Registering $newbie->username" );

		return true;
	}


}