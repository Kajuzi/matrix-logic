<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Handy extends Model
{
    public static function ago($timestamp)
    {
    	$now = new \DateTime('now');
    	$then = new \DateTime($timestamp);

    	$dif = $now->diff($then);

    	if (($dif->y) || ($dif->m) || ($dif->d)) {
    		return $then->format('d M Y');
    	} elseif ($dif->h > 1) {
    		return $dif->h." hours $dif->i mins ago";
    	} elseif ($dif->h == 1) {
    		return "1 hour $dif->i mins ago";
    	} elseif ($dif->i > 1) {
    		return $dif->i." mins ago";
    	} elseif ($dif->i == 1) {
    		return "A minute ago";
    	} else {
    		return "Just now";
    	}    	
    }

    public static function toTime($timestamp)
    {
    	$now = new \DateTime('now');
    	$then = new \DateTime($timestamp);

    	$dif = $now->diff($then);

    	if (($dif->y) || ($dif->m) || ($dif->d)) {
    		return $then->format('d M Y');
    	} elseif (($dif->h <= 23) && ($dif->h > 1)) {
    		return "In $dif->h hours $dif->i mins";
    	} elseif ($dif->h == 1) {
    		return "In 1 hour $dif->i mins";
    	} elseif ($dif->i > 1) {
    		return "In $dif->i mins ago";
    	} elseif ($dif->i == 1) {
    		return "In a minute";
    	} else {
    		return "Almost done";
    	}
    	
    }

    public static function upload($field, $destination = 'uploads') {
      // getting all of the post data
      $file = array($field => \Input::file($field));
      // setting up rules
      $rules = array($field => 'required|mimes:jpeg,jpg,png,pdf|max:500');
      // doing the validation, passing post data, rules and the messages
      $validator = \Validator::make($file, $rules);
      if ($validator->fails()) {
        // send back to the page with the input data and errors
          \Session::flash('toastr', ['type'=>'error', 'msg'=>'Uploaded file is not valid']);
          return false;
      }
      else {
        // checking file is valid.
        if ($file[$field]->isValid()) {
          //$destination = 'uploads'; // upload path
          $extension = $file[$field]->getClientOriginalExtension(); // getting image extension
          $filename = md5(microtime().Auth::user()->id).'.'.$extension; // renaming image
          $file[$field]->move($destination, $filename); // uploading file to given path
          // sending back with message
          \Session::flash('toastr', ['type'=>'success', 'msg'=>'Upload successfully']); 
          return $filename;
        }
        else {
          // sending back with error message.
          \Session::flash('toastr', ['type'=>'error', 'msg'=>'uploaded file is not valid']);
          return false;
        }
      }
    }
}
