<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Settings;
use App\Models\Member;

class Admin extends Model
{
    public static function settings($value='')
    {
    	return Settings::fields();
    }

    public static function membershipSummary()
    {
        $data['all_members'] = \App\Models\Member::all()->count();
        $data['all_pins'] = \App\Models\Pin::all()->count();
        $data['all_managers'] = \DB::table('role_user')->where('role_id', 2)->count();
        $data['new_members'] = \DB::table('users')->orderBy('created_at', 'DESC')->take(5)->get();
        return $data;
    }

    public static function commissionsSummary()
    {
        $members = Member::all(); 
        $data['all_members'] = sizeof($members);
        $data['sum'] = 0;
        foreach ($members as $key => $m) {
            $sum = \App\Models\EwTransaction::commission($m->id)['sum'];
            if ($sum) {
                $data['sum'] += $sum;
            } else {
                $data['all_members']--;
            }            
        }
    	return $data;
    }

    public static function payout()
    {
        $members = Member::all(); 
        foreach ($members as $key => $m) {
            if($m->id == 0)
                continue;
            $sum = \App\Models\EwTransaction::commission($m->id)['sum'];
            if ($sum) {
                $nt = \App\Models\EwTransaction::makeNew(0, $m->id, $sum, 'Commision');
                \App\Models\EwTransaction::approve($nt->id);
            }          
        }
        return true;
    }
}
