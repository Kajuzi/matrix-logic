<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pin;
use App\User;

class Member extends Model
{
    protected $table = 'users';
    private $tree = null;
    private $parent = [];

    public static function newMember()
    {
    	$manager = \Auth::user();
        $pins = Pin::countAvailable($manager->id);
        $user = new User;
        $role = \App\Models\Role::where('name','=','member')->first();;

        if(!$pins){
            return view('errors.general-error', ['msg' =>'You do not have any pins available for this']);
        }

        $a['fname'] = \Input::get('fname');
        $a['lname'] = \Input::get('lname');
        $a['email'] = \Input::get('email');
        $a['username'] = \Input::get('username');
        $a['phone'] = \Input::get('phone');
        $a['password'] = bcrypt(\Input::get('password'));
        $a['active'] = 1;
        $a['manager_id'] = $manager->id;
        $manager_upline = ($manager->upline)?$manager->upline:'|';
        $a['upline'] = $manager_upline.$manager->id.'|';
        $newbie = $user->create($a);
        if ($newbie) {
	        $newbie->attachRole($role);
	        Pin::useOne($manager->id, $newbie);
            if(settings('auto_upgrade')){
                Member::upgrade($manager->id);
            }
	        return $newbie;        	
        } else {
        	return false;
        }
    }
    
    public static function geneologyBranch($id)
    {
        $data = User::find($id)->toArray();
        $data['fruits'] = User::whereRaw("upline LIKE '%$id'")->get();

        return $data;
    }
    
    public static function geneology($id)
    {
        $data = User::find($id)->toArray();
        $data['downline'] = User::whereRaw("upline LIKE '%|$id|'")->get();

        return $data;
    }
    
    public static function simpleDownline($id)
    {
        $data = User::find($id)->toArray();
        $data['downline'] = User::whereRaw("upline LIKE '%|$id|'")->get();
        foreach ($data['downline'] as $key => $m) {
            $data['downline'][$key]->hasDownline = (User::whereRaw("upline LIKE '%|".$m['id']."'")->count()) ? true : false ;
        }
        return $data;
    }
    
    public static function GeneologyTable($id)
    {
        $data['downline'][] = User::find($id)->toArray();
        $downline = User::whereRaw("upline LIKE '%|$id|%'")->get()->toArray();
        $data['downline'] = array_merge($data['downline'], $downline);
        return $data;
    }
    
    public static function GeneologyCount($id)
    {
        return User::whereRaw("upline LIKE '%|$id|%'")->count();
    }
    
    public static function jsonTree($id)
    {
        $m = [];
        $fam = User::whereRaw("upline LIKE '%|$id|%'")->get()->toArray();
        $icons =    [
                        '0' => 'fa fa-user text-gray',
                        '1' => 'fa fa-user text-l1',
                        '2' => 'fa fa-user text-l2',
                        '3' => 'fa fa-user text-l3',
                        '4' => 'fa fa-user text-l4',
                        '4' => 'fa fa-user text-l4',
                        '5' => 'fa fa-user text-l5',
                        '6' => 'fa fa-user text-l6',
                        '7' => 'fa fa-user text-l7',
                        '8' => 'fa fa-user text-l8',
                        '9' => 'fa fa-user text-l9',
                        '10' => 'fa fa-user text-l10',
                        '11' => 'icon-badge text-info-m',
                        '12' => 'icon-diamond  text-info',
                        'm' => 'icon-badge text-info-m',
                        'a' => 'icon-diamond  text-info'
                    ];
        $colours =    [
                        '0' => 'text-gray',
                        '1' => 'text-l1',
                        '2' => 'text-l2',
                        '3' => 'text-l3',
                        '4' => 'text-l4',
                        '4' => 'text-l4',
                        '5' => 'text-l5',
                        '6' => 'text-l6',
                        '7' => 'text-l7',
                        '8' => 'text-l8',
                        '9' => 'text-l9',
                        '10' => 'text-l10',
                        '11' => 'text-info-m',
                        '12' => 'text-info',
                        'm' => 'text-info-m',
                        'a' => 'text-info'
                    ];
        foreach ($fam as $key3 => $row) {            
            $m[] =  [
                        'id' => $row['id'],
                        'parent' => ($row['manager_id'] == $id)?'#':$row['manager_id'],
                        'text' => $row['username'].' <strong><span class="'.$colours[Member::rankId($row['id'])].'">'.$row['fname'].' '.$row['lname'].'</span></strong>',
                        'li_attr' => ['id' => $row['id']],
                        'icon' => $icons[Member::rankId($row['id'])],
                    ];

        }
        return $m;
    }
    
    public static function newMembers($limit = 10)
    {
        $newbies = \DB::table('users')->where('id', '>', 0)->orderBy('created_at','desc')
        					->take($limit)
        					->get();
        foreach ($newbies as $key => $n){        	
    		$newbies[$key]->manager = ($n->manager_id == 0)?'N/A': Member::username($n->manager_id);
    		$newbies[$key]->fullname = Member::fullname($n->id);
    		$newbies[$key]->when = \App\Models\Handy::ago($n->created_at);
        }
        return $newbies;
    }
    
    public static function allMembers($limit = 20)
    {
        $newbies = \DB::table('users')->where('id', '>', 0)->orderBy('created_at','desc')
        					->take($limit)
        					->get();
        foreach ($newbies as $key => $n){        	
    		$newbies[$key]->manager = ($n->manager_id == 0)?'N/A': Member::username($n->manager_id);
    		$newbies[$key]->fullname = Member::fullname($n->id);
    		$newbies[$key]->when = \App\Models\Handy::ago($n->created_at);
        }
        return $newbies;
    }
    
    private function getChildren($id)
    {
        $data['downline'] = User::whereRaw("upline LIKE '%|$id|'")->get();
        foreach ($data['downline'] as $key => $m) {
            $data['downline'][$key]->hasDownline = (User::whereRaw("upline LIKE '%|".$m['id']."'")->count()) ? true : false ;
        }
        return $data;
    }

    /**
    *Returns the full name of a user
    */
    public static function fullname($id)
    {
        $user = User::find($id);
        return $user->fname.' '.$user->lname;
    }

    /**
    *Returns the rank of a user
    */
    public static function rank($id)
    {
        $user = User::find($id);
        $role_id = \DB::table('role_user')->where('user_id', $id)->pluck('role_id');
        switch ($role_id) {
            case 1:
                return "Site Administrator";
                break;
          
            case 2:
                return "Manager";
                break;
          
            case 3:
                return "Member Level $user->level";
                break;
            
            default:
                return 'Member';
                break;
        }
    }

    /**
    *Returns the rank of a user
    */
    public static function rankId($user_id)
    {
        $user = User::find($user_id);
        $role_id = \DB::table('role_user')->where('user_id', $user_id)->pluck('role_id');
        switch ($role_id) {
            case 1:
                return "12";
                break;
          
            case 2:
                return "11";
                break;
          
            case 3:
                return "$user->level";
                break;
            
            default:
                return '0';
                break;
        }
    }

    /**
    *Returns the username of a user
    */
    public static function username($id)
    {
        $user = User::find($id);
        return $user->username;
    }

    /**
    *Checks if user is due for an upgrade and upgrades if necessary
    */
    public static function upgrade($id)
    {
        $user = User::find($id);
        $role_id = \DB::table('role_user')->where('user_id', $id)->pluck('role_id');

        if($role_id == 3 && Member::GeneologyCount($id) > 2){
            if(!$user->level){
                $user->increment('level');
                return true;
            }else{
                $level = \App\Models\Level::find($user->level);
                if(Member::GeneologyCount($id) > $level->downline_count && $user->level < settings('max_member_lev')){
                    $user->increment('level');
                    return true;
                }
            }
            if($user->level < settings('max_member_lev')){
                \DB::table('role_user')->whereRaw("user_id = $user->id and role_id = 3")->update(['role_id' => 2]);
                $user->level = '';
                $user->update();
                return true;
            }else{
                return false;
            }             
        }
        return false;
    }

    /**
    *Checks if user is due for an upgrade and upgrades if necessary
    */
    public static function upgradable($id)
    {
        $user = User::find($id);
        $role_id = \DB::table('role_user')->where('user_id', $id)->pluck('role_id');

        if($role_id == 3 && Member::GeneologyCount($id) > 2){
            if(!$user->level){
                return true;
            }else{
                $level = \App\Models\Level::find($user->level);
                if(Member::GeneologyCount($id) > $level->downline_count && $user->level < settings('max_member_lev')){
                    return true;
                }
            }
            if($user->level < settings('max_member_lev')){
                return true;
            }             
        }
        return false;
    }

    /**
    *Checks if visitor is eitheran admin or in the upline of agiven member
    */
    public static function issuperior($id)
    {
        if(\Entrust::hasRole('admin') && $id != \Auth::id())
            return true;

        $user = User::find($id);
        if($user->upline ==  $id)
            return true;
        return false;
    }

}
