<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $table = 'users';
    public static function userData($id){
    	$bank = \DB::table('member_bank')
                        ->where('member_id', $id)
                        ->first();
        $data = (array)\DB::table('users')
                    ->find($id);                    
        $data['pin'] = Pin::allPins($id);
        $data['ewbalance'] = \App\Models\EwTransaction::balance($id);
        $data['downline'] = \App\Models\Member::GeneologyCount($id);
        if($bank){
            $data['title'] = $bank->title;
            $data['account_num'] = $bank->account_num;            
        }else{
            $data['title'] = '';
            $data['account_num'] = '';            
        }

    	$data['announcements'] = \DB::table('messages')
                                        ->whereRaw('publish_at < NOW() and expires_at > NOW()')
                                        ->orderBy('publish_at', 'DESC')
                                        ->take(5)
                                        ->get();
    	$data['new_members'] = \DB::table('users')->orderBy('created_at', 'DESC')->take(5)->get();
        foreach ($data['new_members'] as $key => $nm) {
            $data['new_members'][$key]->created_at = Handy::ago($nm->created_at);
        }

    	return $data;
    }
}
