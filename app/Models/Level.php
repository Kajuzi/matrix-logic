<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
	public static function ese()
	{
		$all = Level::all();
		$data = [];
		foreach ($all as $key => $m) {
			$data[$m->id] = $m;
		}

		return $data;
	}
	
	public static function updateLevel($r)
	{
		if($r->old_id == 'new')
			$level = new Level;
		else
			$level = Level::find($r->old_id);

		$level->id = $r->id;
		$level->pay = $r->pay;
		$level->pin = $r->pin;
		$level->manager = $r->manager;
		$level->level = $r->level;
		$level->upline = $r->upline;
		$level->downline_count = $r->downline_count;
		$level->total = $r->total;
		$level->profit = $r->profit;
		$level->nett = $r->nett;

		if($level->save())
			return true;
		else
			return false;
	}

	public static function deleteLevel($id)
	{
		if(Level::destroy($id))
			return true;
		else
			return false;
	}
}
