<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Mailbox extends Model
{
	protected $table = 'mailbox';

    /**
    * Returns an array of received message objects
    */
    public static function inbox($id, $page = 1){
    	$messages = \DB::table('mailbox')
    				->whereRaw("recipient_id = $id and status = 'sent' and recipient_trash != 1")
                    ->orderBy('created_at', 'DESC')
    				->get();
    	foreach ($messages as $key => $msg) {
    		$messages[$key]->sender = User::names($msg->sender_id)['username'];
    	}
    	return $messages;
    }

    /**
    * Returns an array of sent message objects
    */
    public static function sent($id = 1, $page = 1){
    	$messages = \DB::table('mailbox')
    				->whereRaw("sender_id = $id and status = 'sent' and sender_trash != 1")
                    ->orderBy('created_at', 'DESC')
    				->get();
        foreach ($messages as $key => $msg) {
            $messages[$key]->sender = User::names($msg->recipient_id)['username'];
        }
    	return $messages;
    }

    /**
    * Returns an array of trashed message objects
    */
    public static function trash($id = 1, $page = 1){
        $messages = \DB::table('mailbox')
                    ->whereRaw("(recipient_id = $id or sender_id = $id) and status = 'sent' and (sender_trash = 1 or recipient_trash = 1)")
                    ->orderBy('created_at', 'DESC')
                    ->get();        
        foreach ($messages as $key => $msg) {
            $messages[$key]->sender = User::names($msg->sender_id)['username'];
        }
        return $messages;
    }

    /**
    * Returns an intiger count of all messages in the inbox
    */
    public static function countUnread($id){
        return \DB::table('mailbox')
                    ->whereRaw("recipient_id = $id and status = 'sent' and recipient_trash != 1 and `read` != 1")
                    ->count();
    }

    /**
    * Returns a message as an object
    */
    public static function singleMessage($id){
        $data = Mailbox::find($id);
        $data->when = \App\Models\Handy::ago($data->created_at);
        $data->sender = \App\Models\Member::fullname($data->sender_id);
        $data->username = \App\Models\Member::username($data->sender_id);
        return $data;
    }

    /**
    * Sends a message to a recipient
    */
    public static function send($recipient_id, $subject, $body){
        if(!is_int($recipient_id))
            $recipient_id = \DB::table('users')->where('username', $recipient_id)->pluck('id');
        $msg = new Mailbox;
        $msg->title = $subject;
        $msg->body = $body;
        $msg->sender_id = \Auth::id();
        $msg->recipient_id = $recipient_id;
        $msg->status = 'sent';
        $msg->save();

        return true;
    }

    /**
    * Delete a message
    * Sends to trash or permanently deletes if message is in trash
    */
    public static function sendToTrash($id){
    	$msg = Mailbox::find($id);
        if($msg->recipient_trash || $msg->sender_trash){
            $msg->delete();
            return true;
        }else{
            if ($msg->sender_id == \Auth::id())
                $msg->sender_trash = 1;                
            else
                $msg->recipient_trash = 1;
            
            $msg->save();
            return true;
        }

        return false;
    }
}
