<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
	protected $table = "settings";
    public static function fields()
    {
    	$settings = [];
    	$raw = Settings::orderBy('order', 'ASC')->get();
    	foreach ($raw as $key => $s) {
    		$input = null;
    		switch ($s->type) {
    		 	case 'boolean':
    		 		$input['input'] = \Form::checkbox($s->slug, 1, (boolean) $s->value, array('class' => 'form-control', 'id' => $s->slug));
    		 		break;
    		 	
    		 	case 'select':
    		 		$options = explode(',',$s->options);
    		 		$options = array_combine($options, $options);
    		 		$input['input'] = \Form::select($s->slug, $options, $s->value, array('class' => 'form-control', 'id' => $s->slug));
    		 		break;
    		 	
    		 	default:
    		 		$input['input'] = \Form::text($s->slug, $s->value, array('class' => 'form-control', 'id' => $s->slug));
    		 		break;
    		}
    		$input['label'] = $s->title;
    		$input['slug'] = $s->slug;
    		$settings[] = $input;
    	}
    	return $settings;
    }

    public static function allToArray()
    {
    	$settings = [];
    	$raw = Settings::all()->orderBy('order', 'ASC')->get();
    	foreach ($raw as $key => $s) {
    		$settings[$s->slug] = $s->value;
    	}
    	return $settings;
    }

    public static function val($slug)
    {
    	$setting = Settings::where('slug', $slug)->first();
    	if (isset($setting->value)) {
    		return $setting->value;
    	} else {
    		return '??';
    	}
    	
    	
    }
}
