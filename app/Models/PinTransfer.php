<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Member;
use App\Models\EwTransaction;

class PinTransfer extends Model
{
    public static function history ($id = null)
    {
        if ($id === null) {
        	$id = \Auth::id();
        }
    	$rows = PinTransfer::whereRaw("sender_id = $id or recipient_id = $id")->orderBy('created_at', 'DESC')->get();
        return PinTransfer::prepRows($id, $rows);
    }

    public static function pending ($id)
    {
    	$rows = PinTransfer::whereRaw("(sender_id = $id or recipient_id = $id) and approved != 1")->get();
    	
        return PinTransfer::prepRows($id, $rows);
    }

    private static function prepRows($id, $rows){
        $transfers = [];
        foreach ($rows as $key => $trnf) {
            $temp = $trnf;
            $temp->approved = ($trnf->approved) ? 'Approved' : 'Pending Approval';
            $temp->amount = ($trnf->sender_id == $id) ? -1 : 1;
            if ($key) {
                if ($trnf->created_at == $transfers[sizeof($transfers) - 1]->created_at && $trnf->sender_id == $transfers[sizeof($transfers) - 1]->sender_id && $trnf->recipient_id == $transfers[sizeof($transfers) - 1]->recipient_id) {
                    $transfers[sizeof($transfers) - 1]->amount += $temp->amount;
                    continue;
                }
            }
            $temp->when = \App\Models\Handy::ago($trnf->created_at);
            $sender = \App\User::find($trnf->sender_id)->username;
            $recipient = \App\User::find($trnf->recipient_id)['username'];
            $temp->other_party = ($trnf->sender_id == $id) ? $recipient :  $sender;
            $transfers[] = $temp;        
        }

        return $transfers;
    }

    public static function approvePurchase($id, $quantity)
    {
        $trxn = PinTransfer::find($id);
        $trxn->approved = 1;
        $trxn->update();
        $pin = Pin::find($trxn->pin_id);
        $pin->available = 1;
        $pin->update();

/*        if($quantity>1){
            dd('ran');
            for ($i=0; $i < $quantity; $i++) { 
                if (!$i) {
                    $ewt = EwTransaction::whereRaw("sender_id = $trxn->sender_id 
                                                    and recipient_id = $trxn->recipient_id")
                                                    //and created_at = '$trxn->created_at'")
                                                ->first();
                    if (!$ewt) {
                        $minute = substr($trxn->created_at, 0, 17);
                        $day = substr($trxn->created_at, 0, 11);
                        $ewt = EwTransaction::whereRaw("sender_id = $trxn->sender_id
                                                    and recipient_id = $trxn->recipient_id
                                                    and created_at like '$day'")
                                                ->first();
                    }
                    echo "$i, ";

                    if (!$ewt)
                        return false;
                }

                $pt = PinTransfer::whereRaw("sender_id = $trxn->sender_id 
                                            and recipient_id = $trxn->recipient_id
                                            and created_at = '$trxn->created_at'
                                            and approved != 1")
                                            ->first();

                Pin::approveTransfer($pt->id);
                $ewt->approved = 1;
                $ewt->update();
            }            
        }*/

        return true;
    }

    public static function declinePurchase($id, $quantity)
    {
        $trxn = PinTransfer::find($id);
        for ($i=0; $i < $quantity; $i++) { 
            if (!$i) {
                $ewt = EwTransaction::whereRaw("sender_id = $trxn->recipient_id
                                                and recipient_id = $trxn->sender_id
                                                and created_at = '$trxn->created_at'")
                                            ->first();
                if (!$ewt) {
                    $minute = substr($trxn->created_at, 0, 17);
                    $ewt = EwTransaction::whereRaw("sender_id = $trxn->recipient_id
                                                and recipient_id = $trxn->sender_id
                                                and created_at like '$minute'")
                                            ->first();
                }

                if (!$ewt)
                    return false;
            }

            $pt = PinTransfer::whereRaw("sender_id = $trxn->sender_id 
                                        and recipient_id = $trxn->recipient_id
                                        and created_at = '$trxn->created_at'
                                        and approved != 1")
                                        ->first();
            $pin = Pin::find($pt->pin_id);
            $pin->available = 1;
            $pin->update();

            $pt->approved = -1;
            $pt->update();
            $ewt->approved = -1;
            $ewt->update();
            alert($ewt->sender_id, "Your request to purchase $quantity pins was declined", 'pins/history', 'error');
        }

        return true;
    }

    public static function requestPurchase($quantity)
    {
        $user = \Auth::user();
        if(EwTransaction::balance($user->id) < $quantity*settings('pin_price'))
            return false;
        $pin_trx = Pin::requestTransfer(Member::username($user->manager_id), $user->username, 'pin transaction', $quantity);
        if(!$pin_trx)
            return false;
        $ew_trx = EwTransaction::makeNew($user->id, $user->manager_id, $quantity*settings('pin_price'), 'pin transaction');
        \App\Models\Alert::makeNew($user->manager_id, "$user->username is requesting to purchase $quantity pins", 'pins/requests');
        return true;
    }

    public static function purchaseRequests($id)
    {
        $rows = PinTransfer::whereRaw("sender_id = $id and approved != 1")->get();
        
        return PinTransfer::prepRows($id, $rows);
    }
}