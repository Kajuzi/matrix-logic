<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'messages';

    public static function makeNew($request)
    {
    	$new = new Announcement;
    	$new->title = $request->title;
        $new->body = $request->body;
    	$new->class = ($request->type)?$request->type:'text-info';
    	$new->expires_at = $request->expires_at;
    	$new->publish_at = $request->publish_at;
    	return $new->save();
    }
}
